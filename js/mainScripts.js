Navigation.controllers['main'] = function(e) {
	$('.clientPubNum>span').html(JSON.parse(localStorage.user)._id);
	$('#general_view #for_app_users').on('click', function() {
		localStorage.realUserType = 'client';
		localStorage.userType = 'user';

		Navigation.changePage('user_main');

		Language.translate();
	});
}