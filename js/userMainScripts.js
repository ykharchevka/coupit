Navigation.controllers['user_main'] = function() {

    if (localStorage.userType == 'user') {
     $('.return_to_client').hide(0);
     // alert(JSON.parse(localStorage.user).role);
    }

    if(localStorage.getItem('firstFacebookLogin') == 'true' && localStorage.getItem('userType') == 'client') {
        Navigation.changePage('settings');
    }

    API._get('/coupons/currUser', function (data) {
        var savedCount = 0;
        for (var i in data.couponsByUser) {
            var coupon = data.couponsByUser[i];
            if (moment(coupon.dateTo).unix() < moment().startOf('day').unix()) {
                continue;
            }
            if (coupon.status == 'onAir')
                savedCount++;
        }
		$('#general_view .saved_coupons_count').text(savedCount);
	});

    API._get('/coupons/currUser/expire', function (data) {
        var savedCount = 0;
        console.log(data);
        for (var i in data.expireCoupons) {
            var coupon = data.expireCoupons[i];
            if (moment(coupon.dateTo).unix() < moment().startOf('day').unix()) {
                continue;
            }
            if (coupon.status == 'onAir')
                savedCount++;
        }
        $('#general_view .expiring_coupons_count').text(savedCount);
	});

	API._get('/coupons/currUser/random', function(data) {
		$('.benefit-slider').html('');
		if (data.randomCoupons.length > 0) {
			// var clientWidth = document.documentElement.clientWidth;
			var clientWidth = 700;
			var coupId;
			var goodCoupons = 0;
			if (document.documentElement.clientWidth < 700) {
				clientWidth = document.documentElement.clientWidth;
			}

			$('#general_view .benefit-slider').css('width', clientWidth);

			$('#general_view .benefit').css('display', '');
			for (var i in data.randomCoupons) {
			    var c = data.randomCoupons[i];
			    coupId = c._id;
			    if (moment(c.dateTo).unix() < moment().startOf('day').unix()) {
			        continue;
			    }
				if (c.status == "onAir") {
				    $('<figure data-id="' + c._id + '" date-to="'+ c.dateTo +'">' +
                        '<div class="benefit-image clearfix" style="background: url(\'' + c.couponImage + '\')">' +
                            '<img width="' + clientWidth + '" alt="' + c.couponName + '" src="' + c.couponImage + '">' +
                            '<span>' + c.couponName + '</span>' +
                            '<div class="benefit-tag">' +
                                '<span><span>' + c.tariff + '</span>₪</span>' +
                                '<span><span>' + c.cellPrice + '</span>₪</span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="benefitslideractions clearfix">'+
                            '<a href="#" id="couponDetailsSlider"><span data-t="coupon_more">Show more</span></a>' +
                            '<a href="#" id="couponWantSlider"><span data-t="give_it_to_me">Save</span></a>' +
                        '</div>'+
                    '</figure>').appendTo('.benefit-slider');
				    goodCoupons++;
				}
				Language.translate();
			}
			if (goodCoupons == 0) {
			    $('#general_view .benefit').hide();
			} else {
			    $('#general_view .benefit').show();
			}
			$('#general_view .benefit-slider figure, #general_view .benefit-slider figure .benefit-image').css('width', clientWidth);
			fontSizer.resize();
			var sliderButtonCLicked = 0;
			$('#general_view a#couponDetailsSlider').on('click', function (e) {
			    e.preventDefault();
			    var id = $(this).parents('figure').data('id');
			    Navigation.changePage('user_coupon', { id: id });
			    sliderButtonCLicked = 1;
			});

			$('#general_view a#couponWantSlider').on('click', function (e) {
			    e.preventDefault();
			    sliderButtonCLicked = 1;
			    var id = $(this).parents('figure').data('id');
			    var couponDateTo = moment($(this).parents('figure').attr('date-to').substr(0, 10).split('-'));
			    var button = $(this);
			    var today = new Date();
			    var currentDate = moment([today.getFullYear(), today.getMonth() + 1, today.getDate()]);
			    var daysToExpire = couponDateTo.diff(currentDate, 'days') + 1;

			    API._post('/coupons/currUser/favourite', function (data) {
			        //console.log(data);

			        if (data.isCouponFavourite == true) {
			            button.addClass('saved');
			            button.find('span').data('t', 'use_it');
			        } else {
			            button.removeClass('saved');
			            button.find('span').data('t', 'give_it_to_me');
			        }

			        Navigation.alert(Language.scheme.messages.use_it_before_alert + daysToExpire + Language.scheme.messages.use_it_after_alert, function () {
			            Navigation.changePage('user_coupons');
			        });
			    }, { couponId: id }, false);
			    Language.translate();
			});

			$('#general_view .benefit-slider figure').on('click', function () {
			    if (sliderButtonCLicked == 0) {
			        var id = $(this).data('id');
			        Navigation.changePage('user_coupon', { id: id });
			    }
			    sliderButtonCLicked = 0;
			});



			if ($.isFunction($.fn.cycle)) {

				$('#general_view .benefit-slider').cycle({pager:'#general_view .dots',fx:'scrollHorz',slideExpr:'figure',next:'#next',prev:'#prev'});
				$('#general_view .benefit-slider').touchwipe({
					wipeLeft: function(e) {
						e.preventDefault();
						$('#general_view .benefit-slider').cycle('next');
					},
					wipeRight: function(e) {
						e.preventDefault();
						$('#general_view .benefit-slider').cycle('prev');
					},
					preventDefaultEvents: false
				});
			}
		} else {
			$('#general_view .benefit').css('display', 'none');
		}
	});

	if (localStorage.realUserType == 'client') {
		$('#general_view .return_to_client').css('display', '').on('click', function(e) {
			e.preventDefault();

			delete localStorage.realUserType;
			localStorage.userType = 'client';

			Navigation.changePage('main');
		});
	}
    if(JSON.parse(localStorage.user).role != 'client'){
        $('#general_view .return_to_client').css('display', 'none');
    }
}