﻿Navigation.controllers['forgot'] = function() {
    $('#general_view .ok').hide();
    $('#general_view .error').hide();

    $('#general_view #send').click(function () {
        if ($('#general_view #email').val()) {
            API._post('/users/password/forgot', function(e, statusCode) {
                if (statusCode == 200) {
                    if (e.notFound) {
                        $('#general_view .ok').hide();
                        $('#general_view .error').show();
                    }
                    else {
                        $('#general_view .error').hide();
                        $('#general_view .ok').show();

                        Navigation.alert(Language.scheme.alerts.password_send_to_your_email);

                        setTimeout(function(){
                            document.location.href = "#login";
                        }, 3000);
                    }
                } else {
                    Navigation.alert(Language.scheme.alerts.email_is_wrong);
                    $('#general_view #email').val('');
                }
            }, {email: $('#general_view #email').val()});
        } else {
            Navigation.alert(Language.scheme.alerts.please_type_email);
        }
    });
};