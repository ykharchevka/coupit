Navigation.controllers['end'] = function(data) {
	// var cData = data.formData;
	var oData = data.objectData;
	var edit = data.edit;
	var paymentInfo = '';
    console.log('oData',oData);

	if (typeof oData['paymentId'] != 'undefined') {
		API._get('/payments/creditGuard/' + oData['paymentId'], function(e) {
			paymentInfo = e.payment.cardId.replace(/.*(\d{4})/,'*$1') + ' ' + e.payment.type + ' (' + e.payment.cardHolder + ')';
		}, {}, false);
		$('#general_view .value[data-name="paymentInfo"]').text(paymentInfo);
	} else {
		$('#general_view .value[data-name="paymentInfo"]').html('&ndash;');
	}

	$('#general_view .value').each(function() {
		if (typeof oData[$(this).data('name')] !== 'undefined') {
			$(this).text( oData[$(this).data('name')] );

			if($(this).data('name') == 'radius' && typeof localStorage.generalBenefit !== 'undefined') {
				$('.general_area').show();
			}

		} else {
			$(this).html('<span style="color: red">'+ Language.scheme.alerts.none + '</span>');
        }
	});
	if (paymentInfo && typeof oData['paymentId'] != 'undefined') {
		$('#general_view .value[data-name="paymentInfo"]').text(paymentInfo);
	} else {
		$('#general_view .value[data-name="paymentInfo"]').html('&ndash;');
	}
	$('#general_view form input[type="submit"]').on('click', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();

		delete localStorage.createCoupon;
		delete localStorage.editCoupon;
		delete localStorage.editId;
		delete localStorage.generalBenefit;
		delete localStorage.skippedPayment;

		Navigation.alert_continue(Language.scheme.alerts.benefit_create_successfully, function(){
            Navigation.changePage('main');
        });
	});
	var payDate = function () {
        var today = new Date();
	    var currentDay = today.getDate();
	    var currentMonth = today.getMonth() + 1;
	    var currentYear = today.getFullYear();

	    if (currentDay < 29) {
	        currentDay = 28;
	    } else {
	        currentDay = 28;
	        if (currentMonth < 12) {
	            currentMonth += 1;
	        } else {
	            currentMonth = 1;
	            currentYear += 1;
	        }
	    }

	    if (currentMonth < 10) {
	        currentMonth = '0' + currentMonth;
	    }
	    today = currentDay + '/' + currentMonth + '/' + currentYear;
	    return today;
	};
	$('#general_view .value[data-name="paymentDate"]').text(payDate);
};