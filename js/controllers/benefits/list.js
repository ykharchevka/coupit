var bindCouponsEvents = function(data) {
    //         if (typeof c.payment == 'undefined') {
    //             $('#general_view #details_but[data-id="' + c._id + '"]').attr('data-t','add_payment').attr('id','addPayment');
    //         }

    // Bind coupon editing button
    $("#general_view #updateCoupon").on('click', function () {
        var couponId = $(this).attr('data-id');
        Navigation.changePage('edit_coupon', {id: couponId});
    });

    // Bind coupon pausing button
    $("#general_view #pauseCoupon").on('click', function () {
        var couponId = $(this).attr('data-id');

        Navigation.confirm(Language.scheme.alerts.put_coupon_on_hold, function () {
            API._put('/coupons/' + couponId + '/pause', function (e) {
                Navigation.alert_continue(Language.scheme.messages.coupon_paused, function () {
                    Navigation.changePage('coupons');
                });
            });
        },
        function () {

        });

    });

    $("#general_view #deleteCoupon").on('click', function (e) {
        var couponId = $(this).attr('data-id');
        e.preventDefault();
        e.stopImmediatePropagation();

        Navigation.confirm(Language.scheme.alerts.are_you_sure_delete_coupon, function() {
            API._delete('/coupons/' + couponId, function(data) {
                if (!data.isCouponDelete) {
                    Navigation.alert(Language.scheme.alerts.something_wrong_try_again);
                }

                Navigation.changePage('coupons');
            });
        });
    });

    $("#general_view #coupons-page a.coupon").on('click', function () {
        var couponId = $(this).data('id');
        Navigation.changePage('coupon', {id: couponId});
    });

    $("#general_view #coupons-page button#details_but").on('click', function () {
        var couponId = $(this).data('id');
        Navigation.changePage('details', { id: couponId });
    });

    $("#general_view #coupons-page button#addPayment").on('click', function () {
        Navigation.changePage('charge');
    });

    $("#general_view #coupons-page a .goUpdateCoupon").on('click', function(e) {
        e.stopImmediatePropagation();

        var couponId = $(this).parent().data('id');
        console.log(couponId);
        Navigation.changePage('edit_coupon', {id: couponId});
    });
}

Navigation.controllers['coupons'] = function() {
    API._get('/coupons/client', function (data) {
        Navigation.renderPage('couponsPage', { coupons: data.couponsByClient }, function() {
            bindCouponsEvents(data);
        });
    });
};