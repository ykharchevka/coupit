function hideFields() {
    $('#general_view fieldset:has(input[name=email])').hide();
    $('#general_view fieldset:has(input[name=companyName])').hide();
}

function showFields() {
    $('#general_view fieldset:has(input[name=email])').show();
    $('#general_view fieldset:has(input[name=companyName])').show();
}

function hidePayPalButton() {
    $('#ipaypal .btnw').hide();
    $('.paypal-connected').show();
}

function showPayPalButton() {
    $('#ipaypal .btnw').show();
    $('.paypal-connected').hide();
}

function checkPayPalData() {
    var userId = JSON.parse(localStorage.getItem('user'))._id;

    API._get('/clients/' + userId + '?expandPayments=true', function(data) {
        var payments = data.user.payments;
        var payPalData = payments.find(function(el) {
            return el.service === 'paypal';
        });

        localStorage.setItem('payments',JSON.stringify(payments));

        if (payPalData) {
            hidePayPalButton();
        } else {
            showPayPalButton()
        }
    }, {}, true);
}

function openPayPalWindow() {
    var userId = JSON.parse(localStorage.getItem('user'))._id;

    console.log('userId',userId);

    var popupParams = 'status=1, height=400, width=500,location=no';
    var url = API.ipAddress + '/auth/paypal?userId=' + userId;
    var popup = window.open(url, '_blank', popupParams);
    var callbackUrlRegexp = /paypal\/callback/;

    // InAppBrowser specific event.
    // Visit http://docs.phonegap.com/en/edge/cordova_inappbrowser_inappbrowser.md.html for additional info

    popup.addEventListener('loadstop', function(e) {
        if (e.url.match(callbackUrlRegexp)) {
            popup.close();

            checkPayPalData();
        }
    });
}

// For additional info visit http://livr-spec.org/

var creditCardRules = {
    cardType:        ['required', { 'one_of': ['visa','mastercard'] } ],
    numberFull:      ['required', { min_length: 16, max_length: 16} ],
    firstName:       'not_empty',
    lastName:        'not_empty',
    companyName:     ['required', { min_length: 1, max_length: 50}],
    email:           ['required', 'email'],
    cvv:             ['required', { max_number: 999, min_number: 1}],
    insuranceNumber: ['required', { max_length: 50}],
};

var payPalRules = {
    companyName:     ['required', { min_length: 1, max_length: 50}],
    email:           ['required', 'email'],
    email:           ['required', 'email'],
};

var fieldAlerts = {
    companyName:     'payment_company_name',
    email:           'payment_email',
    cvv:             'payment_cvv',
    firstName:       'payment_firstName',
    lastName:        'payment_lastName',
    numberFull:      'payment_wrongCard',
    insuranceNumber: 'payment_insuranceNumber',
};


function showErrorFields(errors) {
    var validationText = Language.scheme.alerts.payment_before + '<br />';
    var $activeTab = $('#general_view .tab-payment#icard').parent();

    $activeTab.find('fieldset').removeClass('invalid');

    _.forEach(errors, function(error,fieldName) {
        $activeTab.find('input[name="' + fieldName + '"]').closest('fieldset').addClass('invalid');

        validationText += Language.scheme.alerts[fieldAlerts[fieldName]] + '<br />';
    });

    validationText += Language.scheme.alerts.payment_after;

    var invalidField = $('.invalid input').get(0);
    var offset = $(invalidField).offset();

    if (typeof invalidField !== 'undefined') {
        Navigation.alert(validationText, function(){
            setTimeout(function(){
                $('.invalid input').get(0).focus();
            }, 100);
        });
    }

    return;
}

function addCreditCard(formData,data) {

    // For additional info visit http://livr-spec.org/

    var validator = new LIVR.Validator(creditCardRules);

    var isValidated = validator.validate(formData);
    var errors = validator.getErrors();

    console.log('formData',formData);
    window.sessionStorage.removeItem('payment_data');
    window.sessionStorage.setItem('payment_data', JSON.stringify(formData));

    var validationText = Language.scheme.alerts.payment_before + '<br />';

    if (!isValidated) {
        showErrorFields(errors);

        return;
    }

    formData.currency = 'USD' // Добавил валюту, т.к. в форме ее нету
    formData.number = parseInt(formData.numberFull);
    formData.expireYear = formData.expireYear.substr(2, 2);;

    formData.cardHolder = formData.firstName + ' ' + formData.lastName;
    delete formData.firstName;
    delete formData.lastName;

    API._post('/payments/creditGuard', function(e,statusCode,responseText) {
        console.log('e',e);
        console.log('statusCode', statusCode);
        console.log('responseText',responseText);

        if (statusCode === 500) { // This means that the external service returned card error
            if (responseText.match(/Defective card/)) {
                Navigation.alert(Language.scheme.alerts.wrong_credit_card);
            }
            if (responseText.match(/Expired card/)) { // TODO: Change alert for expired card
                Navigation.alert(Language.scheme.alerts.wrong_credit_card);
            }

            // TODO: Add alerts for each case.
            // Server could return other alerts
            Navigation.alert(Language.scheme.alerts.wrong_credit_card);

            return;
        }

        if (data) { // If you add a payment method inside benefit creation
            Navigation.changePage('end', {objectData: data.data.objectData, edit: data.data.edit});
        } else {
            console.info('NO data');
            Navigation.changePage('settings');
        }
    }, formData);

    return false;
}

function addPayPal(formData,data) {
    var payments = JSON.parse(localStorage.getItem('payments'));
    var payPalData = payments.find(function(el) {
        return el.service === 'paypal';
    });

    if (!payPalData) {
        Navigation.alert(Language.scheme.alerts.paypal_not_connected);

        return;
    }

    formData.serviceUserId = payPalData.userId;
    formData.type = 'paypal';

    var validator = new LIVR.Validator(payPalRules);
    var isValidated = validator.validate(formData);
    var errors = validator.getErrors();

    if (!isValidated) {
        showErrorFields(errors);

        return;
    }

    API._post('/payments/paypal', function(e,statusCode,responseText) {
        //TODO: Add 400 and 500 error handlers

        if (data) { // If you add a payment method inside benefit creation
            Navigation.changePage('end', {objectData: data.data.objectData, edit: data.data.edit});
        } else {
            console.info('NO data');
            Navigation.changePage('settings');
        }
    }, formData);

    return false;
}


// These functions were moved here while refactoring
// TODO: Refactoring, refactoring, refactoring!

var bindPageEvents = function(data) {
    checkPayPalData();

    $('#general_view .skip').on('click', function(e) {
        if (typeof data != 'undefined') {
            Navigation.changePage('end', data.data);
            localStorage.setItem('skippedPayment', true) ;
        } else {
            Navigation.changePage('main');
        }
        e.preventDefault();
        return false;
    });

    if (Navigation.prevPage == 'end') {
        var restoredPaymentData = JSON.parse(window.sessionStorage.getItem("payment_data"));
    }

    $('#general_view input').focus( function(e) {
        var offset = $(this).offset();
        offset.top -= 250;
        $("html, body").animate({
            scrollTop: offset.top
        }, "slow");
    });

    $('#general_view .tab-payment').hide();
    $('#general_view .tab-payment-choose>div:first').addClass('active').find('input').attr('checked','');
    $('#general_view .tab-payment:first').show();
    $('#general_view .tab-payment-choose>div').on('click', function() {
        $('#general_view input').removeClass('oops');
        $('#general_view input').removeClass('oops');
        $('#general_view .tab-payment-choose>div').removeClass('active');
        $('#general_view .tab-payment-choose>div').find('input').attr('checked','');
        $(this).addClass('active').find('input').attr('checked','checked');
        $('#general_view .tab-payment').hide();

        var activeTab = $(this).find('input:radio').val();
        $('#general_view #' + activeTab).fadeIn();
    });

    for (var i = 1; i <= 12; i++) {
        var month_value = (i < 10) ? '0' + i : i;
        $('#general_view .tab-payment#icard select[name="expireMonth"]').append('<option value="' + month_value + '">' + i + '</option>')
    }

    var curYear = parseInt(moment().format('YYYY'));
    for (var i = 0; i <= 10; i++) {
        var year = curYear + i + '';
        $('#general_view .tab-payment#icard select[name="expireYear"]').append('<option value="' + year + '">' + year + '</option>')
    }

    if (Navigation.prevPage == 'end') {
        var restoredPaymentData = JSON.parse(window.sessionStorage.getItem("payment_data"));

        $('#general_view .creditCardNumberFull').val(restoredPaymentData.numberFull);
        $('#general_view input[name=cvv]').val(restoredPaymentData.cvv);
        $('#general_view input[name=firstName]').val(restoredPaymentData.firstName);
        $('#general_view input[name=lastName]').val(restoredPaymentData.lastName);

        $('#general_view input[name=insuranceNumber]').val(restoredPaymentData.insuranceNumber);
        $('#general_view input[name=companyName]').val(restoredPaymentData.companyName);
        $('#general_view input[name=email]').val(restoredPaymentData.email);

        $('#general_view select[name=expireMonth] option[value=' + restoredPaymentData.expireMonth + ']').attr("selected", "selected");
        $('#general_view select[name=expireYear] option[value=20' + restoredPaymentData.expireYear + ']').attr("selected", "selected");
        $('#general_view select[name=cardType] option[value=' + restoredPaymentData.cardType + ']').attr("selected", "selected");
    }

    $("#general_view #ipaypal a").on('click', function (e) {
        e.preventDefault();
        openPayPalWindow();
        return false;
    });

    $('#general_view #i_agree').change(function () {
        var user = JSON.parse(localStorage.user);

        if ($(this).is(":checked")) {
            hideFields();
            $('#general_view input[name=email]').val(user.email);
            $('#general_view input[name=companyName]').val(user.businessName);
        } else {
            showFields();
            $('#general_view input[name=email]').val('');
            $('#general_view input[name=companyName]').val('');
        }

    });

    $('#general_view form').on('submit', function(e) {
        e.preventDefault();

        var dataArray = $(this).serializeArray();
        var formData = serializedToObject(dataArray);

        if (formData.payment === 'ipaypal') {
            addPayPal(formData,data);
        } else {
            addCreditCard(formData,data);
        }
    });
}


Navigation.controllers['payment'] = function(data) {
    Navigation.renderPage('paymentPage',data,bindPageEvents);
};