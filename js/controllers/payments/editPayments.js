Navigation.controllers['edit_payments'] = function() {
    API._get('/payments', function(data) {
        if (data.payments.length != 0) {
            for (var i in data.payments) {
                var p = data.payments[i];

                var type = '';
                if (typeof p['cardId'] != 'undefined') {
                    type = 'card';
                }

                switch(type) {
                    case 'card':
                        $('#general_view #cards_list').append('<option value="' + p._id + '">' + p.cardId + '</option>');
                    break;
                }
            }
        }
    });

    $('#general_view form').on('submit', function(e) {
        e.preventDefault();

        var cardId = $('#general_view #cards_list').val();

        Navigation.changePage('edit_payment', cardId);
    });
}