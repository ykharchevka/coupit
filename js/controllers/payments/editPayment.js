Navigation.controllers['edit_payment'] = function(id) {

    if (typeof id == 'undefined') {
        Navigation.changePage('main');
        return false;
    }

    for (var i = 1; i <= 12; i++) {
        var month_value = (i < 10) ? '0' + i : i;
        $('#general_view .tab-payment#icard select[name="expireMonth"]').append('<option value="' + month_value + '">' + i + '</option>')
    }

    var curYear = parseInt(moment().format('YYYY'));
    for (var i = 0; i <= 10; i++) {
        var year = curYear + i + '';
        // var year_value = year.substr(2);
        $('#general_view .tab-payment#icard select[name="expireYear"]').append('<option value="' + year + '">' + year + '</option>')
    }

    var paymentData;
    API._get('/payments/' + id, function(data) {
        paymentData = data.payment;
    }, {}, false);

    paymentData['cvv'] = 'XXX';

    if (paymentData['expireMonth'] < 10) {
        paymentData['expireMonth'] = '0' + paymentData['expireMonth'];
    }

    $('#general_view form').find('input[type!=submit], select').each(function() {
        var obj = $(this);
        var name = $(obj).attr('name');

        obj.val(paymentData[name]);
    });

    $('#general_view form').on('submit', function(e) {
        e.preventDefault();

        var serializedForm = $(this).serializeArray();
        var data = serializedToObject(serializedForm);

        delete data['cardNumber'];
        delete data['cvv'];

        API._put('/payments/' + id, function(e) {
            console.log(e);
        }, data);
    });
};
