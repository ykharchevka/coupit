
Navigation.controllers['charge'] = function(data) {
    Navigation.renderPage('chargePage', data, function() {
        $('#general_view .skip').on('click', function(e) {
            if (typeof data != 'undefined') {
                Navigation.changePage('end', {objectData: data.objectData, edit: data.edit});
                localStorage.setItem('skippedPayment', true) ;
            } else {
                Navigation.changePage('main');
            }
            e.preventDefault();
            return false;
        });

        if (typeof localStorage.orderId != 'undefined') {
            var orderId = localStorage.orderId;
            delete localStorage.orderId;

            Navigation.showLoader(undefined, Language.scheme.messages.payment_processing);
            var orderApproved = false;

            var orderInterval = setInterval(function() {
                API._get('/orders/' + orderId, function(e) {
                    if (e.order.state == 'approved') {
                        orderApproved = true;
                        Navigation.hideLoader();
                        clearInterval(orderInterval);
                    }
                }, false);
            }, 2000);

            return false;
        }

        $("#general_view #backToCreatingCoupon").on('click', function () {
            Navigation.changePage('limit');
        });

        $('#general_view #existing_payments').on('click', function() {
            Navigation.changePage('existing_payment', {coupon: true, data: data});
        });

        $('#general_view #new_payment').on('click', function() {
            Navigation.changePage('payment', {coupon: true, data: data});
        });
    });
};