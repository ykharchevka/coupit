Navigation.controllers['existing_payment'] = function(coupon_data) {
    Navigation.renderPage('existingPaymentPage', coupon_data, function() {
        API._get('/payments', function(data) {
            $('#general_view #cards_fieldset select option').remove();

            if (data.notFound == true) {
                Navigation.changePage('payment');
            }

            if (data.payments.length != 0) {
                for (var i in data.payments) {
                    var p = data.payments[i];

                    var type = '';
                    if (typeof p['cardId'] != 'undefined') {
                        type = 'card';
                    } else {
                        type = 'paypal';
                    }

                    switch(type) {
                        case 'card':
                            $('#general_view #cards_fieldset select').append('<option value="' + p._id + '">' + p.type + ' ' + p.cardId + '</option>');
                        break;
                        case 'paypal':

                        break;
                    }
                }
            }
        });

        $('#general_view form').on('submit', function(e) {
            e.preventDefault();

            var objectData = coupon_data.data.objectData;
            var checked_payment = $('#general_view input[name="payment"]:checked');

            var paymentId = '';

            var failure = false;

            if (checked_payment.length > 0) {
                switch(checked_payment.attr('id')) {
                    case 'card':
                        paymentId = $('#cards_fieldset select option:selected').val();
                    break;
                    case 'paypal':
                        Navigation.showLoader(undefined, Language.scheme.messages.redirecting_to_paypal);

                        API._post('/orders', function(order) {
                            if (typeof order['order'] != 'undefined') {
                                window.location = order.order.urls.approve;
                            }
                        }, {
                            paymentType: 'paypal',
                            amount: objectData.maxBudget,
                            currency: Language.scheme.config.currency_code,
                            couponId: objectData.couponId
                        });

                        return false;
                    break;
                }
            }

            if (typeof paymentId == 'undefined' || paymentId == '') {
                failure = true;
            }

            objectData['paymentId'] = paymentId;

            if (!failure) {
                Navigation.alert_continue(Language.scheme.alerts.payment_method_will_be_used_to_pay, function(){
                    Navigation.changePage('end', {objectData: objectData, edit: coupon_data.data.edit});
                });
            } else {
                Navigation.alert(Language.scheme.alerts.payment_not_chosen);
            }
        });
    });
};