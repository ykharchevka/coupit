Navigation.controllers['messages'] = function() {
    API._get('/messages',function(data) {
        Navigation.renderPage('messagesPage', { messages: data.messages });
    });
};