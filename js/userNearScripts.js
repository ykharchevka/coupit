Navigation.controllers['user_near'] = function() {
	loadLocalBenefits();

	$('.near-top-update').on('click', loadLocalBenefits);
}

var loadLocalBenefits = function() {
	// Populate categories dropdown list
	API._get('/coupons/categories?lang=' + Language.language, function(data) {
		$('#general_view .near-top-edit form').html('');
		$('#general_view .filter-icons').html('');

        for (var i in data.categories) {
            var c = data.categories[i];

            $('#general_view .near-top-edit form').append('<fieldset><input checked="checked" id="near' + c._id + '" data-cat_id="' + c._id + '" type="checkbox" /><label for="near' + c._id + '">' + c.name + '</label></fieldset>');
            $('#general_view .filter-icons').append('<a href="javascript:void(0);" data-id="' + c._id + '"><i>' + c.icon + '</i><span>' + c.name + '</span></a>')
        }

        $('#general_view .filter-icons a').on('click', function(e) {
			var id = $(this).data('id');

			Navigation.changePage('category', {id: id});
		});
    }, {}, false);

	navigator.geolocation.getCurrentPosition(
		function(position) {
			//alert(position.coords.latitude + '-' + position.coords.longitude);
			API._get('/coupons/near', function (data) {
                var localBenefits = data.couponsNear;
				var userCouponsIds = [];
				var archivedCouponsIds = [];

				API._get('/coupons/currUser', function(data) {
					if (data.couponsByUser.length > 0) {
						for (var i in data.couponsByUser) {
							userCouponsIds.push(data.couponsByUser[i]._id);
						}
					}
				}, {}, false);

				API._get('/coupons/currUser/archive', function(data) {
	                if (data.archiveCouponsByUser.length > 0) {
	                    for (var i in data.archiveCouponsByUser) {
	                        archivedCouponsIds.push(data.archiveCouponsByUser[i]._id);
	                    }
	                }
	            }, {}, false);

				$('#general_view .near-top-update .refreshed_time').text(moment().format('HH:mm'));

				// var catsFilter = $('#general_view .near-top-edit form');
				// catsFilter.find('fieldset').remove();
				var goodCoupons = 0;
				var goodCategories = 0;
                console.log('localBenefits',localBenefits);

				if (localBenefits.length > 0) {
		        	for (var cat in data.couponsNear) {
		        		if (data.couponsNear[cat].coupons.length > 0) {
		        			var cat_block = $(
		        				'<div class="near-item-row" data-cat_id="' + data.couponsNear[cat]._id + '">' +
								'<h3 class="category-header"><i>B</i><span>' + data.couponsNear[cat].name + '</span></h3>' +
								'<div class="near-slider clearfix"></div></div>');

		        			for (var c in data.couponsNear[cat].coupons) {
		        				var coupon = data.couponsNear[cat].coupons[c];

		        				if (moment(coupon.dateTo).unix() < moment().startOf('day').unix()) {
		                            continue;
		                        }
		        				if (coupon.status == "onAir") {
		        				    goodCoupons++;
		        				    var saveButton = '<a class="near-item-save"><i>v</i><span data-t="give_it_to_me">תן לי את זה</span></a>';
		        				    // If this coupon is already bookmarked - change "save" button style
		        				    if (userCouponsIds.indexOf(coupon._id) != -1) {
		        				        saveButton = '<a class="near-item-save saved"><i>v</i><span data-t="i_already_have">תן לי את זה</span></a>';
		        				    }

		        				    if (archivedCouponsIds.indexOf(coupon._id) != -1) {
		        				        saveButton = '<a class="near-item-save archived"><i>v</i><span data-t="archived"></span></a>';
		        				    }

		        				    cat_block.find('.near-slider').append($('<div>' +
									    '<div class="near-slider-item clearfix" data-id="' + coupon._id + '">' +
										    '<img alt="" src="' + coupon.couponImage + '">' +
										    '<h3>' + coupon.couponName + '</h3>' +
										    '<b>' + coupon.MarketingStatement + '</b>' +
										    '<span class="near-item-desc"><span data-t="valid_until">בתוקף עד</span>: ' + moment(coupon.dateTo).format('D.M.YYYY') + '</span>' +
										    '<div class="near-slider-bottom">' +
											    '<span class="near-price">' +
												    '<span class="near-price-old price_elm"><span>₪</span><span>' + coupon.tariff + '</span></span>' +
												    '<span class="near-price-new price_elm"><span>₪</span><span>' + coupon.cellPrice + '</span></span>' +
											    '</span>' +
										    '</div>' +
										    '<a class="near-item-more show_coupon_details" href="#" data-t="coupon_more">לפרטים</a>' +
										    saveButton +
									    '</div>' +
								    '</div>'));
		        				}
		        			}
		        			if (goodCoupons != 0) {
		        			    goodCategories++;
		        			    $('#general_view .near-items').append(cat_block);
		        			    goodCoupons = 0;
		        			} else {
                               // don't add category
		        			}
		        		}
		        		if (!goodCategories) {
		        		    $('#general_view .near-items').append($('<div id="noBenefits">' + Language.scheme.messages.no_coupons + '</div>'));
		        		}
		        	}
		        } else {
                    $('#general_view .near-items').append($('<div id="noBenefits">' + Language.scheme.messages.no_coupons + '</div>'));
                }

	        	Language.translate();

	        	$('#general_view .show_coupon_details').on('click', function(e) {
	        		e.preventDefault();
	        		e.stopImmediatePropagation();

	        		var id = $(this).parents('.near-slider-item').data('id');
	        		Navigation.changePage('user_coupon', {id: id});
	        	});

	        	$('#general_view .near-item-save').on('click', function(e) {
	                e.stopImmediatePropagation();

	                var button = $(this);
	                var id = $(button).parents('.near-slider-item').data('id');

	                if (button.hasClass('archived')) {
	                    return false;
	                }

	                if (button.hasClass('saved')) {
	                    API._post('/coupons/currUser/archive', function(data) {
	                        button.removeClass('saved').addClass('archived');
	                        button.find('span').data('t', 'archived');

	                        Navigation.alert_ok(Language.scheme.alerts.thank_you_for_using_benefit, function() {
	                            Navigation.changePage('user_coupons');
	                        });
	                    }, {couponId: id}, false);
	                } else {
	                    API._post('/coupons/currUser/favourite', function(data) {
	                        console.log(data);

	                        if (data.isCouponFavourite == true) {
	                            button.addClass('saved');
	                            button.find('span').data('t', 'use_it');
	                        } else {
	                            button.removeClass('saved');
	                            button.find('span').data('t', 'give_it_to_me');
	                        }
	                    }, {couponId: id}, false);
	                }

	                Language.translate();
	            });

	        	$('#general_view .near-top-edit input[type="checkbox"]').on('change', function(e){

	        		var id = $(this).data('cat_id');
					console.log('cat_id', id)
	        		var object = $('#general_view .near-item-row[data-cat_id="' + id + '"]');
					console.log('object', object)

	        		if ($(this).is(':checked')) {
	        			object.show();
	        		} else {
	        			object.hide();
	        		}
	        	});

				var direction = $('body').attr('direction');
	            var rtl = false;
	            if (direction == 'rtl') {
	                rtl = true;
	            }

	            $("#general_view .near-slider").owlCarousel({lazyLoad:true,stagePadding:20,items:3,rtl:rtl,responsive:{0:{items:1},360:{items:2},600:{items:3}}});
	        }, {lat: position.coords.latitude, lng: position.coords.longitude, categories: 'true'});
		},
		function(error) {
			// $('.loaderDiv').fadeOut(300);
			console.log(error);
		},
		{timeout: 10 * 1000, enableHighAccuracy: true}
	);
}