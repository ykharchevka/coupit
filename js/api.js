var API = {
    backendUrl: window.config.hostName,

	request: function(type, method, success, data) {
        data = data || {};

		var self = this;

		var requestData = {
			type: type,
			url: self.backendUrl + method,
			xhrFields: {
				withCredentials: true
			},
			data: data,
			cache: false,
            processData: true,
			complete: function(xhr, status) {
				var data = xhr.responseJSON;
				var responseText = xhr.responseText;
				var statusCode = xhr.status;

				if (statusCode == 0) {
					Navigation.alert(Language.scheme.alerts.network_error, function() {
						Navigation.changePage('no_internet');
					});

					return false;
				}

				if (statusCode == '401') {
					Navigation.changePage('login');
				} else {
					if (typeof success == 'function') {
						success(data, statusCode, responseText);
					}
				}
			},
            error: function(e) {
                console.error('Error',e);
			},
			async: true
		};

		if (data instanceof FormData) {
			requestData.processData = false;
			requestData.contentType = false;
		} else {
			if (localStorage.user && data['userId'] != false) {
				data['userId'] = JSON.parse(localStorage.user)._id;
			}
		}

		if (data['userId'] == false) {
			delete data['userId'];
		}

        $.ajax(requestData);
	},
	_post: function(method, success, data) {
		this.request('POST', method, success, data);
	},
	_get: function(method, success, data) {
        this.request('GET', method, success, data);
	},
	_delete: function(method, success, data) {
		this.request('DELETE', method, success, data);
	},
	_put: function(method, success, data) {
		this.request('PUT', method, success, data);
	},
    _patch: function(method, success, data) {
        this.request('PATCH', method, success, data);
    }
}