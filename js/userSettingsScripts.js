Navigation.controllers['user_settings'] = function(data) {
    var user = JSON.parse(localStorage.user);

    if(localStorage.getItem('firstFacebookLogin') == 'true') {
        setTimeout(function() {
            var profile = $('.settings-list h3').get(0);
            profile.click();
            localStorage.setItem('firstFacebookLogin', false);
            $('#userSettingsUsername').focus();
        }, 1000);
    }

    if (typeof data != 'undefined' && typeof data['key'] != 'undefined') {
        var key = data['key'];
        $('#general_view form[data-skey=' + key + ']').css('display', 'block');
        $('#general_view form[data-skey!=' + key + ']').css('display', 'none');
        $('#general_view form[data-skey!=' + key + ']').prev('h3').css('display', 'none');

    }

    $('#general_view input[name="username"]').val(user.username);
    $('#general_view input[name="email"]').val(user.email);

    $('#general_view .settings-list .profile_id').text('(' + JSON.parse(localStorage.user).username + ')');

    if (user.publish_to_facebook == 1) {
        $('#general_view input[name=post_to_fb]').attr('checked', 'checked');
    }

    $("#general_view #sendSupportRequest").click(function () {
        var formData = {};
        var SF = $('#general_view #support_form').serializeArray();

        for (var i in SF) {
            formData[SF[i].name] = SF[i].value;
        }

        if (!formData.email || !formData.fullName || !formData.message || !formData.phone) {
            Navigation.alert(Language.scheme.alerts.all_fields_required);
            return;
        }

        if (!validateEmail(formData.email)) {
            Navigation.alert(Language.scheme.alerts.email_is_wrong);
            return;
        }

        API._post('/clients/supportRequest',
            function(e) {
                var result = false;

                if (e.isAllFieldsSend === false) {
                    Navigation.alert(Language.scheme.alerts.all_fields_required);
                } else if (e.isMailSend) {
                    Navigation.alert(Language.scheme.alerts.support_requrest_was_sent);
                    result = true;
                } else if (!e.isMailSend) {
                    Navigation.alert(Language.scheme.alerts.support_requrest_was_sent);
                    result = true;
                }

                if (result) {
                    Navigation.alert_continue(Language.scheme.alerts.support_requrest_was_sent);
                    $('#general_view #support_form [name="email"]').val('');
                    $('#general_view #support_form [name="fullName"]').val('');
                    $('#general_view #support_form [name="message"]').val('');
                    $('#general_view #support_form [name="message"]').text('');
                    $('#general_view #support_form [name="phone"]').val('');
                }
            },
            formData
        );
    });

    $('#save_settings').on('submit', function(e) {
        e.preventDefault();

        var formData = {};
        var serializedData = [];

        formData = {
            username: $('#general_view #general_form input[name="username"]').val(),
            password: $('#general_view #general_form input[name="password"]').val(),
            newPassword: $('#general_view #general_form input[name="newPassword"]').val(),
            // newPasswordCheck: $('#general_view #general_form input[name="passwordCheck"]').val(),
            // email: JSON.parse(localStorage.user).email,
            email: $('#general_view #general_form input[name="email"]').val(),
            // newEmailCheck: $('#general_view #general_form input[name="emailCheck"]').val(),
        };


        if (!formData.email || !validateEmail(formData.email)) {
            Navigation.alert(Language.scheme.alerts.emails_must_be_filled);
            return false;
        }

        if (!checkSpecialEmptiness(formData.password, formData.newPassword)) {
            Navigation.alert(Language.scheme.alerts.passwrods_must_be_filled);
            return false;
        }

        var method = localStorage.userType + 's';

        var savingData = {
            username: formData.username,
            password: formData.password,
            newPassword: formData.newPassword,
            email: JSON.parse(localStorage.user).email,
            // newEmail: formData.newEmail,
        }

        var oldPasswordIsValid = false;

        if (savingData.password == '' || savingData.newPassword == '') {
            delete savingData.password;
            delete savingData.newPassword;
        } else {
            // Check password before changing it
            API._post('/sessions', function(e, statusCode) {
                if (e.notFound || statusCode == 400) {

                } else {
                    oldPasswordIsValid = true;
                    savingData.password = savingData.newPassword;
                    delete savingData.newPassword;
                }
            }, {email: savingData.email, password: savingData.password}, false);
        }

        if (!oldPasswordIsValid) {
            Navigation.alert(Language.scheme.alerts.username_or_password_is_wrong);
            return false;
        }

        API._put('/users/' + user._id, function(data, statusCode) {
            if (data.user && typeof data.user !== 'undefined') {
                localStorage.user = JSON.stringify(data.user);
            }

            if (statusCode == 400) {
                Navigation.alert('check_fields_or_try_later');
            } else {
                console.log('saved')
                Navigation.alert('changes_saved', function() {
                    Navigation.changePage('user_main');
                });
            }
        }, savingData);
    });

    function buildTranslateButtons() {
        var languages = Language.getLanguages();

        $('#general_view #languages_list').html('');

        for (var i in languages) {
            $('#general_view #languages_list').append($('<a href="javascript:void(0);" class="btng small" data-lang="' + i + '">' + languages[i] + '</a>'));
        }

        $('#general_view #languages_list a').on('click', function(e) {
            var language = $(this).data('lang');

            Language.init(language).translate();
            buildTranslateButtons();
        });
    }

    buildTranslateButtons();
}