﻿Navigation.controllers['registration'] = function() {
    $('#general_view .skip_registration').on('click', function() {
        API._post('/clients', function(e) {
            if (e.idExists) {

            }

            API._post('/sessions', function(f) {
                localStorage.login = true;
                localStorage.user = JSON.stringify(f.user);
                localStorage.userType = f.user.role;

                if (localStorage.userType == 'client') {
                    Navigation.changePage('main');
                } else {
                    Navigation.changePage('user_main');
                }
            }, {id: deviceId});

        }, {id: deviceId});
    });

	$("#general_view #facebook_link").on('click', function(data) {
        var userType = 'CUSTOMER';

		$('.fbHandler').trigger('click', userType);
	});

    API._get('/coupons/categories?lang=' + Language.language, function(data) {
        for (var i in data.categories) {
            var c = data.categories[i];

            $('<input name="businessCategories" type="checkbox" id="id_' + c._id + '" value="' + c._id + '"><label for="id_' + c._id + '">' + c.name + '</label>').appendTo('#general_view #categories .half:eq(' + (i % 2) + ')');
        }
    }, {}, false);

    $('#general_view input[type!=submit][type!=button]').on('change blur', function() {
        var input = $(this);
        var fieldset = input.parents('fieldset');
        var name = input.attr('name');
        var value = input.val();

        var valid = true;
        switch(name) {
            case 'country':
            case 'street' :
            case 'streetNumber' :
            case 'cell' :
            case 'businessPhone':
                valid = (value != '');
            break;

            case 'businessCategories':
                valid = ($('#general_view input[name=businessCategories]:checked').length <= 2);
            break;

            case 'username':
                valid = (value.length != '' && value.length <= 30);
            break;

            case 'city' :
            case 'businessName' :
                valid = (value.length >= 2);
            break;

            case 'email':
            case 'emailCheck':
                valid = validateEmail(value);
            break;
            case 'password':
            case 'passCheck':
                valid = (value.length >= 3 && value.length <= 20);
            break;
        }

        if (valid) {
            fieldset.attr('class', 'valid');
        } else {
            fieldset.attr('class', 'invalid');
        }
    });

    $('#general_view #register').on('click', function () {
        var pass1 = $('#general_view [name = password]').val(),
            pass2 = $('#general_view [name = passCheck]').val();
            email1 = $('#general_view [name = email]').val(),
            email2 = $('#general_view [name = emailCheck]').val();
            UName = $('#general_view [name = username]').val(),
            businessName = $('#general_view [name = businessName]').val(),
            country = $('#general_view [name = country]').val(),
            city = $('#general_view [name = city]').val(),
            street = $('#general_view [name = street]').val(),
            streetNumber = $('#general_view [name = streetNumber]').val(),
            phoneNumber = $('#general_view [name = cell]').val(),
            businessPhone = $('#general_view [name = businessPhone]').val(),
            businessCategories = [];

            $('#general_view input[name=businessCategories]:checked').each(function(i){
                businessCategories[i] = $(this).val();
            });

        if (UName && UName != '') {
            if (pass1 && pass1 != '') {
                if (pass2 != pass1) {
                    $('#general_view [name = password]').val('').focus().parent().addClass('invalid');
                    $('#general_view [name = passCheck]').val('');
                    Navigation.alert(Language.scheme.alerts.passwords_not_same);
                    return false;
                } else if (email1 && email1 != '') {
                    if (email2 != email1) {
                        $('#general_view [name = email]').val('').focus().parent().addClass('invalid');
                        $('#general_view [name = emailCheck]').val('').parent().addClass('invalid');
                        Navigation.alert(Language.scheme.alerts.emails_not_same);
                        return false;
                    } else if (validateEmail(email1)) {

                        //other fields required too
                        if(businessName === '') {
                            $('#general_view [name = businessName]').focus().parent().addClass('invalid');
                            Navigation.alert(Language.scheme.alerts.businessName_must_be_filled);
                            return false;
                        } else if(businessCategories.length > 2) {
                            $('#general_view [name = businessCategories]').focus().parent().addClass('invalid');
                            Navigation.alert(Language.scheme.messages.benefit_cats_limit);
                            return false;
                        } else if(country === '') {
                            $('#general_view [name = country]').focus().parent().addClass('invalid');
                            Navigation.alert(Language.scheme.alerts.country_must_be_filled);
                            return false;
                        } else if (city === '' && city.length < 2) {
                            $('#general_view [name = city]').focus().parent().addClass('invalid');
                            Navigation.alert(Language.scheme.alerts.city_must_be_filled);
                            return false;
                        } else if (street === '') {
                            $('#general_view [name = street]').focus().parent().addClass('invalid');
                            Navigation.alert(Language.scheme.alerts.street_must_be_filled);
                            return false;
                        } else if (streetNumber === '') {
                            $('#general_view [name = streetNumber]').focus().parent().addClass('invalid');
                            Navigation.alert(Language.scheme.alerts.streetNumber_must_be_filled);
                            return false;
                        } else if (phoneNumber === '') {
                            $('#general_view [name = cell]').focus().parent().addClass('invalid');
                            Navigation.alert(Language.scheme.alerts.phoneNumber_must_be_filled);
                            return false;
                        } else if (businessPhone === '') {
                            $('#general_view [name = businessPhone]').focus().parent().addClass('invalid');
                            Navigation.alert(Language.scheme.alerts.businessPhone_must_be_filled);
                            return false;
                        } else {
                            // Everything is OK
                            ajx();
                        }

                    } else {
                        $('#general_view [name = email], #general_view [name = emailCheck]').val('').focus().parent().addClass('invalid');
                        Navigation.alert(Language.scheme.alerts.email_is_wrong);
                        return false;
                    }
                } else {
                    $('#general_view [name = email]').focus().parent().addClass('invalid');
                    Navigation.alert(Language.scheme.alerts.emails_must_be_filled);
                    return false;
                }
            } else {
                $('#general_view [name = password]').focus().parent().addClass('invalid');
                Navigation.alert(Language.scheme.alerts.passwrods_must_be_filled)
                return false;
            }
        } else {
            $('#general_view [name = username]').focus().parent().addClass('invalid');
            Navigation.alert(Language.scheme.alerts.username_required);
            $('#general_view [name = username]').val('');
            return false;
        }
    });

    //alert('register');
    function ajx() {
        var formDataRaw = $('#general_view #registrationForm').serializeArray();
        var formData = {};

        var skippedFields = ['passCheck', 'emailCheck', 'businessCategories'];

        if (formDataRaw.length > 0) {
            for (var i in formDataRaw) {
                if (skippedFields.indexOf(formDataRaw[i].name) === -1) {
                    formData[formDataRaw[i].name] = formDataRaw[i].value;
                }
            }
        }

        formData['businessCategories'] = [];
        $('#general_view input[name=businessCategories]:checked').each(function() {
            formData['businessCategories'].push($(this).val());
        });

        formData['businessCategories'] = formData['businessCategories'].join(',');

        formData['businessKind'] = 'asd';

        console.log(formData);

        API._post('/clients', function(e, statusCode) {
            $('#general_view input').removeClass('oops');

            if (statusCode == 400) {
                Navigation.alert(Language.scheme.alerts.all_fields_required);

                for (var i in e) {
                    $('#general_view input[name="' + e[i]['param'] + '"]').addClass('oops');
                }

                return false;
            }

            if (e.emailExists) {
                Navigation.alert(Language.scheme.alerts.email_already_used);
            } else if (e.usernameExists) {
                Navigation.alert(Language.scheme.alerts.login_already_used);
            } else {
                localStorage.registered = true;
                // alert(Language.scheme.alerts.registration_successfull);

                API._post('/sessions', function(e) {
                    localStorage.login = true;
                    localStorage.user = JSON.stringify(e.user);
                    localStorage.userType = e.user.role;

                    Navigation.changePage('main');
                }, {email: formData.email, password: formData.password});
            }
        }, formData);
    }
};