// Multilanguage support
var LanguageScheme = {};

var Language = {
	defaultLanguage: 'he',
	language: '',
	scheme: {},
	init: function(language) {
		if (language === undefined) {
			if (typeof localStorage['language'] != 'undefined') {
				this.language = localStorage['language'];
			} else {
				this.language = window.navigator.language.substr(0, 2);
			}
		} else {
			this.language = language;
		}

		if (LanguageScheme[this.language] === undefined) {
			this.language = this.defaultLanguage;
		}

		this.scheme = LanguageScheme[this.language];

		return this;
	},
	translate: function() {
		var scheme = this.scheme;

		// Change direction styles and apply specific class for changing margins etc
		jQuery('body').attr('direction', scheme.config.direction);

		// Translate messages
		jQuery('[data-t]').each(function() {
			var key = jQuery(this).data('t');
			jQuery(this).html(scheme.messages[key]);
		});

		// Translate values
		jQuery('[data-t-v]').each(function() {
			var key = jQuery(this).data('t-v');
			jQuery(this).attr('value', scheme.values[key]);
		});

		for (var i in scheme) {
			if (i != 'config') {
				jQuery('[data-t-' + i + ']').each(function() {
					var key = jQuery(this).data('t-' + i);
					jQuery(this).text(scheme[i][key]);
				});
			}
		}
	},
	getLanguages: function() {
		var languages = {};

		for (var i in Language.scheme.languages) {
			if (i != this.language) {
				languages[i] = Language.scheme.languages[i];
			}
		}

		return languages;
	}
};