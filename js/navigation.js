window.Navigation = {
    menuHiddenAt: ['home'],
    loaderTime: 0,
    curPage: '',
    prevPage: '',
    showLoader: function(callback, text) {
        if (text != '' && typeof text != 'undefined') {
            $('.loaderDiv #loaderText').text(text);
            $('.loaderDiv').addClass('puls');
        }

        $('.loaderDiv, .loaderDiv #loaderImg').css('display', 'block');
        $('.loaderDiv #loaderImg').css('opacity', 1);
        $('.loaderDiv').fadeIn(this.loaderTime, function() {
            if (typeof callback == 'function') {
                callback();
            }
        });

    },
    hideLoader: function(callback) {
        setTimeout(function() {
            $('.loaderDiv #loaderImg').fadeOut(this.loaderTime, function() {
                $('.loaderDiv #loaderText').text('');
                $('.loaderDiv').removeClass('puls');

                $('.loaderDiv').fadeOut(this.loaderTime, function() {
                    $('.loaderDiv').css('display', 'none');

                    if (typeof callback == 'function') {
                        callback();
                    }
                });
            });
        }, this.loaderTime);
    },
    alert: function(message, onClose) {
        if (typeof Language.scheme.messages[message] !== 'undefined') {
            $('#mp_popup #text').text('').data('t', message);
        } else {
            $('#mp_popup #text').html(message);
        }

        $('#mp_popup .white-popup-nav a').off('click');
        $('#mp_popup .white-popup-nav a').on('click', function(e) {
            e.stopImmediatePropagation();

            if (typeof onClose === 'function') {
                onClose();
            }
        });

        Language.translate();

        $.magnificPopup.open({items: {src: '#mp_popup', type: 'inline'}});
    },
    alert_set_coords_manually: function(message, onClose) {

        if (typeof Language.scheme.messages[message] !== 'undefined') {
            $('#mp_popup_set_manually #text').text('').data('t', message);
        } else {
            $('#mp_popup_set_manually #text').html(message);
        }

        $('#mp_popup_set_manually .white-popup-nav a').off('click');
        $('#mp_popup_set_manually .white-popup-nav a').on('click', function(e) {
            e.stopImmediatePropagation();

            if (typeof onClose === 'function') {
                onClose();
            }
        });

        Language.translate();

        $.magnificPopup.open({items: {src: '#mp_popup_set_manually', type: 'inline'}});
    },
    alert_ok: function(message, onClose) {
        if (typeof Language.scheme.messages[message] !== 'undefined') {
            $('#mp_popup_ok #text_ok').text('').data('t', message);
        } else {
            $('#mp_popup_ok #text_ok').html(message);
        }

        $('#mp_popup_ok .white-popup-nav a').off('click');
        $('#mp_popup_ok .white-popup-nav a').on('click', function(e) {
            e.stopImmediatePropagation();

            if (typeof onClose === 'function') {
                onClose();
            }
        });

        Language.translate();

        $.magnificPopup.open({items: {src: '#mp_popup_continue', type: 'inline'}});
    },
    alert_continue: function(message, onClose) {
        if (typeof Language.scheme.messages[message] !== 'undefined') {
            $('#mp_popup_continue #text').text('').data('t', message);
        } else {
            $('#mp_popup_continue #text').html(message);
        }

        $('#mp_popup_continue .white-popup-nav a').off('click');
        $('#mp_popup_continue .white-popup-nav a').on('click', function(e) {
            e.stopImmediatePropagation();

            if (typeof onClose === 'function') {
                onClose();
            }
        });

        Language.translate();

        $.magnificPopup.open({items: {src: '#mp_popup_continue', type: 'inline'}});
    },
    confirm: function(message, onConfirm, onClose) {
        if (typeof Language.scheme.messages[message] !== 'undefined') {
            $('#mp_confirm #text').text('').data('t', message);
        } else {
            $('#mp_confirm #text').html(message);
        }

        $('#mp_confirm .white-popup-nav a.confirm').off('click');
        $('#mp_confirm .white-popup-nav a.confirm').on('click', function(e) {
            e.stopImmediatePropagation();

            if (typeof onConfirm === 'function') {
                onConfirm();
            }
        });

        $('#mp_confirm .white-popup-nav a.cancel').off('click');
        $('#mp_confirm .white-popup-nav a.cancel').on('click', function(e) {
            e.stopImmediatePropagation();

            if (typeof onClose === 'function') {
                onClose();
            }
        });

        Language.translate();

        $.magnificPopup.open({items: {src: '#mp_confirm', type: 'inline'}});
    },
    init: function() {
        this.goBack = function() {
            switch (localStorage.userType) {
                case 'user':
                    Navigation.changePage('user_main');
                    break;
                case 'client':
                    Navigation.changePage('main');
                    break;
            }
        };

        var self = this;

        jQuery(document).on('click', 'a.goto_page', function(e) {
            e.preventDefault();

            var id = jQuery(this).attr('href').substr(1);
            self.changePage(id);
        });

        var defaultPage = jQuery('body').data('default_page');

        if (defaultPage === undefined) {
            defaultPage = jQuery('[data-role="page"]:eq(0)').attr('id').split('_')[0];
        }

        this.changePage(defaultPage);

        // Used for imitating "back" button
        window.onpopstate = function(e) {
            // console.log(Navigation.curPage);

            // if (Navigation.curPage == 'home') {
            // 	return false;
            // }

            self.changePage(location.hash.substr(1), e.state, true);
        }
    },
    renderPage: function(pageName,pageData,callback) {
        // New templating system that loads templates from separate template files.
        // It's not the best solution, but maybe better than the previous one in the current circumstances

        Navigation.showLoader();

        $.get('templates/' + pageName + '.html', function(templateString) {
            var template = _.template(templateString);
            console.log('pageData',pageData);
            var result = template({data: pageData});

            $('#general_view').html(result);

            Navigation.hideLoader();
            Language.translate();

            if (callback) {
                callback(pageData);
            }
        }, 'html');
    },
    changePage: function(id, data, preventPushing) {
        var self = this;

        Navigation.showLoader();

        $.magnificPopup.close();

        // If user is logged in and trying to access guest page
        if (localStorage.login == 'true' && this.acm_login_required.guest.indexOf(id) != -1) {
            switch (localStorage.userType) {
                case 'client':
                    Navigation.changePage('main');
                    break;
                case 'user':
                    Navigation.changePage('user_main');
                    break;
            }

            return false;
        }

        if (
            localStorage.userType != 'admin' &&
            (
                (!localStorage.userType && this.acm_login_required.guest.indexOf(id) == -1) ||
                (localStorage.userType != undefined &&
                this.acm_login_required[localStorage.userType].indexOf(id) == -1 &&
                this.acm_login_required.guest.indexOf(id) == -1)
            )
        ) {
            this.changePage('login');
            Navigation.alert(Language.scheme.alerts.no_access);
            console.log('no access')
            return false;
        }
        this.prevPage = this.curPage;
        this.curPage = id;

        this.showLoader(function() {
            // jQuery('#general_view').html(jQuery('#' + id + '_page[data-role="page"]').html());

            jQuery('#general_view')[0].innerHTML = '';
            jQuery('#general_view')[0].innerHTML = jQuery('#' + id + '_page[data-role="page"]').html();

            if (self.controllers[id] !== undefined && typeof self.controllers[id] == 'function') {
                self.controllers[id](data);
            }

            self.controllers['general']();

            Language.translate();

            self.hideLoader();
        });

        if (this.menuHiddenAt.indexOf(id) !== -1) {
            jQuery('#sliding_menu').css('display', 'none');
        } else {
            jQuery('#sliding_menu').css('display', 'block');
        }

        if(this.curPage == 'settings' || this.curPage == 'user_settings' || this.curPage == 'unautorizedSettings'){
            $('#homePageLink').show(0);
        } else {
            $('#homePageLink').hide(0);
        }

        setTimeout(function() {
            $(window).scrollTop( 0 );
        }, 50);

        if (typeof preventPushing === 'undefined' || preventPushing === false) {
            // Used for imitating "back" button
            history.pushState(data, id, '#' + id);
        }
    },
    goBack: function() {

    },
    controllers: {},
    acm_login_required: {
        guest: [
            'home',
            'login',
            'user_registration',
            'registration',
            'forgot',
            'facebookAuthorization',
            'unautorizedTerms',
            'unautorizedPrivacy',
            'unautorizedSettings'
        ],
        user: [
            'user_main',
            'user_near',
            'user_coupons',
            'user_expiring_coupons',
            'user_coupon',
            'user_settings',
            'settings',
            'user_comments',
            'coupon',
            'all_coupons',
            'comments',
            'category',
            'terms',
            'privacy',
            'no_internet'
        ],
        client: [
            'main',
            'settings',
            'coupons',
            'details',
            'create_coupon',
            'edit_coupon',
            'limit',
            'data',
            'charge',
            'payment',
            'existing_payment',
            'user_settings',
            'end',
            'completion',
            'comments',
            'user_history',
            'coupon',
            'all_coupons',
            'summary',
            'existing_payments',
            'edit_payments',
            'edit_payment',
            'messages',
            'terms',
            'privacy',
            'no_internet'
        ]
    }
};