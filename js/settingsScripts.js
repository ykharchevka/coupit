﻿Navigation.controllers['settings'] = function(data) {
    var user = JSON.parse(localStorage.user);
    $('#gotoTermsPage').on('click', function(e){
        e.preventDefault();
        Navigation.changePage('terms');
        return false;
    });

    for (var i in user) {
        if ($('#general_view input[name="' + i + '"]').length > 0) {
            $('#general_view input[name="' + i + '"]').val(user[i]);
        }
    }

    if (typeof data != 'undefined' && typeof data['key'] != 'undefined') {
        var key = data['key'];
        $('#general_view form[data-skey=' + key + ']').css('display', 'block');
        $('#general_view form[data-skey!=' + key + ']').css('display', 'none');
        $('#general_view form[data-skey!=' + key + ']').prev('h3').css('display', 'none');

    }

    API._get('/coupons/categories?lang=' + Language.language, function(data) {
        for (var i in data.categories) {
            var c = data.categories[i];

            $('<input name="category" type="checkbox" id="id_' + c._id + '" value="' + c._id + '"><label for="id_' + c._id + '">' + c.name + '</label>').appendTo('#general_view #businessCategories .half:eq(' + (i % 2) + ')');
        }
    }, {}, false);

    $('#general_view input[name=receiveSms]').attr('checked', user.receiveSms);
    $('#general_view input[name=receiveEmail]').attr('checked', user.receiveEmail);
    $('#general_view input[name=receivePush]').attr('checked', user.receivePush);

    $('#general_view .settings-list .profile_id').text('(' + JSON.parse(localStorage.user).username + ')');

    $('#general_view #general_form #save_settings').on('click', function(e) {
        e.preventDefault();

        var formDataRaw = $('#general_view #general_form').serializeArray();
        var formData = formDataComplete = {};

        var skippedFields = ['passCheck', 'emailCheck', 'oldPassword'];
        var skippedFieldsData = {};

        var oldPasswordIsValid = false;

        if (formDataRaw.length > 0) {
            for (var i in formDataRaw) {
                formDataComplete[formDataRaw[i].name] = formDataRaw[i].value;
            }

            if (formDataComplete.password == '' || formDataComplete.newPassword == '') {
                delete formDataComplete.password;
                delete formDataComplete.newPassword;
            } else {
                // Check password before changing it
                API._post('/sessions', function(e, statusCode) {
                    if (e.notFound || statusCode == 400) {

                    } else {
                        oldPasswordIsValid = true;
                        formDataComplete.password = formDataComplete.newPassword;
                        delete formDataComplete.newPassword;
                    }
                }, {email: formDataComplete.email, password: formDataComplete.password}, false);
            }

            for (var i in formDataComplete) {
                if (skippedFields.indexOf(i) !== -1) {
                    delete formDataComplete[i];
                }
            }
        }

        if (!oldPasswordIsValid) {
            Navigation.alert(Language.scheme.alerts.username_or_password_is_wrong);
            return false;
        }

        API._put('/clients/' + user._id, function(data, statusCode) {
            if (data.user && typeof data.user !== 'undefined') {
                localStorage.user = JSON.stringify(data.user);
            }

            if (statusCode == 400) {
                Navigation.alert('check_fields_or_try_later');
            } else {
                Navigation.alert(Language.scheme.values.save_settings, function() {
                    Navigation.changePage('main');
                });
            }
        }, formData);
    });

    $("#general_view #messagesSettingsSubmit").on('submit', function (e) {
        e.preventDefault();

        var formData = {
            receiveSms : $(this).find('input[name = receiveSms]').is(':checked'),
            receiveEmail : $(this).find('input[name = receiveEmail]').is(':checked')
        };

        API._put('/clients/' + user._id, function(data, statusCode) {

            if (statusCode == 400) {
                Navigation.alert('check_fields_or_try_later');
            } else {
                data.user.receiveSms = formData.receiveSms;
                data.user.receiveEmail = formData.receiveEmail;

                Navigation.alert('changes_saved', function() {
                    localStorage.user = JSON.stringify(data.user);
                    Navigation.changePage('settings');
                });
            }
        }, formData);
    });

    $("#general_view #sendSupportRequest").click(function () {
        var formData = {};
        var SF = $('#general_view #support_form').serializeArray();

        for (var i in SF) {
            formData[SF[i].name] = SF[i].value;
        }


        if (!formData.email || !formData.fullName || !formData.message || !formData.phone) {
            Navigation.alert(Language.scheme.alerts.all_fields_required);
            return;
        }

        if (!validateEmail(formData.email)) {
            Navigation.alert(Language.scheme.alerts.email_is_wrong);
            return;
        }

        API._post('/clients/supportRequest',
            function(e, statusCode) {
                var result = false;

                if (statusCode == 400) {
                    Navigation.alert('check_fields_or_try_later');
                } else {
                    /*if (e.isAllFieldsSend === false) {
                        Navigation.alert(Language.scheme.alerts.all_fields_required);
                    } else */if (e.isMailSend) {
                        Navigation.alert(Language.scheme.alerts.support_requrest_was_sent, function(){
                            Navigation.changePage('main');
                        });

                        result = true;
                    }/* else if (!e.isMailSend) {
                        Navigation.alert(Language.scheme.alerts.support_requrest_was_sent);
                        result = true;
                    }*/
                }

                if (result) {
                    $('#general_view #support_form [name="email"]').val('');
                    $('#general_view #support_form [name="fullName"]').val('');
                    $('#general_view #support_form [name="message"]').val('');
                    $('#general_view #support_form [name="message"]').text('');
                    $('#general_view #support_form [name="phone"]').val('');
                }
            },
            formData
        );

    });


    // $("#SaveSettings").click(function () {
    //     var formData = new FormData();
    //     var SF = $('#general_view #allSettings form').serializeArray();
    //     SF["publishOnFacebook"] = $("#general_view [name='publishOnFacebook']").is(':checked');
    //     SF["getingSms"] = $("#general_view [name='getingSms']").is(':checked');
    //     SF["getingEmail"] = $("#general_view [name='getingEmail']").is(':checked');

    //     $.ajax({
    //         type: "POST",
    //         url: ipAddress + "/businessClientSettings",
    //         xhrFields: {
    //             withCredentials: true
    //         },
    //         data: SF,
    //         success: function (e) {
    //             alert(Language.scheme.alerts.info_was_saved);
    //         }, error: function (err) {
    //             //alert('אופס... משהו השתבש. אנא נסה שנית.');
    //         }

    //     });
    // });


    function buildTranslateButtons() {
        var languages = Language.getLanguages();var languages = Language.getLanguages();

        $('#general_view #languages_list').html('');

        for (var i in languages) {
            $('#general_view #languages_list').append($('<a href="javascript:void(0);" class="btng small" data-lang="' + i + '">' + languages[i] + '</a>'));
        }

        $('#general_view #languages_list a').on('click', function(e) {
            var language = $(this).data('lang');

            Language.init(language).translate();
            buildTranslateButtons();

            localStorage.language = language;
        });
    }

    buildTranslateButtons();
};