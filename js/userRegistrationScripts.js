Navigation.controllers['user_registration'] = function() {
    $('#general_view .skip_registration').on('click', function() {
        API._post('/users', function(e) {
            if (e.idExists) {
                
            }

            API._post('/sessions', function(f) {
                localStorage.login = true;
                localStorage.user = JSON.stringify(f.user);
                localStorage.userType = f.user.role;

                if (localStorage.userType == 'client') {
                    Navigation.changePage('main');
                } else {
                    Navigation.changePage('user_main');
                }
            }, {id: window.deviceId});

        }, {id: window.deviceId});
    });
    
	$('#general_view #registrationForm').on('submit', function(e) {
		e.preventDefault();
	});

    $('#general_view input').on('change blur', function() {
        var input = $(this);

        var fieldset = input.parents('fieldset');
        var name = input.attr('name');
        var value = input.val();

        var valid = true;
        switch(name) {
            case 'username':
                valid = (value != '');
            break;
            case 'email':
            case 'emailCheck':
                valid = validateEmail(value);
            break;
            case 'password':
            case 'passCheck':
                valid = (value.length >= 8 && value.length <= 20);
            break;
        }

        if (valid) {
            fieldset.attr('class', 'valid');
        } else {
            fieldset.attr('class', 'invalid');
        }
    });

	$('#general_view #user_register').on('click', function() {
        var pass1 = $('#general_view input[name = password]').val(), pass2 = $('#general_view [name = passwordCheck]').val();
        var email1 = $('#general_view [name = email]').val(), email2 = $('#general_view [name = emailCheck]').val();
        var UName = $('#general_view [name = username]').val();

        if (UName && UName != '') {
            if (pass1 && pass1 != '') {
                if (pass2 != pass1) {
                    Navigation.alert(Language.scheme.alerts.passwords_not_same);
                    $('#general_view [name = password]').val('');
                    $('#general_view [name = passwordCheck]').val('');
                } else if (email1 && email1 != '') {
                    if (email2 != email1) {
                        Navigation.alert(Language.scheme.alerts.emails_not_same);
                        $('#general_view [name = emailCheck]').val('');
                    } else if (validateEmail(email1)) {
                        ajx();
                    } else {
                        Navigation.alert(Language.scheme.alerts.email_is_wrong);
                    }
                } else {
                    Navigation.alert(Language.scheme.alerts.emails_must_be_filled)
                }
            } else {
                Navigation.alert(Language.scheme.alerts.passwrods_must_be_filled)
            }
        } else {
            Navigation.alert(Language.scheme.alerts.username_required)
        }
	});

    function ajx() {
        var requiredFields = ['username', 'email', 'password'];
        var formDataRaw = $('#general_view #registrationForm').serializeArray();
        var formData = {};

        for (var i in formDataRaw) {
            if (requiredFields.indexOf(formDataRaw[i].name) !== -1) {
                formData[formDataRaw[i].name] = formDataRaw[i].value;
            }
        }

        API._post('/users', function(e, statusCode) {
            $('#general_view input').removeClass('oops');

            if (statusCode == 400) {
                Navigation.alert(Language.scheme.alerts.all_fields_required);

                for (var i in e) {
                    $('#general_view input[name="' + e[i]['param'] + '"]').addClass('oops');
                }

                return false;
            }

            if (e.emailExists) {
                Navigation.alert(Language.scheme.alerts.email_already_used);
            } else if (e.usernameExists) {
                Navigation.alert(Language.scheme.alerts.login_already_used);
            } else {
                localStorage.registered = true;
                // alert(Language.scheme.alerts.registration_successfull);

                API._post('/sessions', function(e) {
                    localStorage.login = true;
                    localStorage.user = JSON.stringify(e.user);
                    localStorage.userType = e.user.role;

                    Navigation.changePage('user_main');
                }, {email: formData.email, password: formData.password});
            }
        }, formData);
    }
};
