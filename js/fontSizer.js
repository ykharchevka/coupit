var fontSizer = {
	presets: [
		{
			selector: '#general_view .benefit-tag .old_price',
			def: 50,
			min: 5,
			factor: 6,
		},
		{
			selector: '#general_view .benefit-tag .old_price',
			def: 36,
			min: 8,
			factor: 6,
		},
		{
			selector: '#general_view .benefit-tag > span:first-child',
			def: 24,
			min: 4,
			factor: 3,
		},
		{
			selector: '#general_view .benefit-tag > span:first-child + span',
			def: 50,
			min: 4,
			factor: 9,
		}
	],
	resize: function() {
		var self = this;

		for (var i in this.presets) {
			var preset = this.presets[i];

			var collection = $(preset.selector);

			if (collection.length > 0) {
				collection.each(function() {
					if ($(this).text().length > preset.min) {
						$(this).css('fontSize', (preset.def - (preset.factor * ($(this).text().length - preset.min))));
					}
				});
			}
		}
	}
}