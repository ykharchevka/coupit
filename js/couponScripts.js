Navigation.controllers['coupon'] = function(data) {
    var couponId = undefined;

    if (data && data.id) {
        couponId = data.id;
    }

    // Get coupon data
    API._get('/coupons/' + couponId, function (e) {
        // if (e.coupon.pause) {
        //     $('#general_view #pauseCoupon span').data('t', 'cp_unpause_coupon');
        //     Language.translate();
        // }

        //var ownerId = e.coupon.couponOwner;
        //
        //if (e.coupon.couponOwner.indexOf(',') !== -1) {
        //    ownerId = e.coupon.couponOwner.split(',')[0];
        //}

        //if (ownerId != JSON.parse(localStorage.user)._id) {
        //    Navigation.changePage('main');
        //}

        if (e.coupon) {
            $("#general_view .couponNumber").text(e.coupon.couponNumber);
            $("#general_view .couponDescription").text(e.coupon.couponDescription);
            $("#general_view .dateTo").text(dateFormat(e.coupon.dateTo));
            $("#general_view .creditBalance").text(e.coupon.maxBudget);
            $("#general_view .couponImage").attr("src", e.coupon.couponImage);

            $('#general_view #coupon_counters tr td.numberOfViews').text(e.coupon.views);
            $('#general_view #coupon_counters tr td.numberOfClicks').text(e.coupon.clicks);
            $('#general_view #coupon_counters tr td.numberOfShares').text(e.coupon.shares);
            $('#general_view #coupon_counters tr td.numberOfCalls').text(e.coupon.calls);
            $('#general_view #coupon_counters tr td.numberOfSiteClicks').text(e.coupon.siteClicks);
            $('#general_view #coupon_counters tr td.numberOfFacebookClicks').text(e.coupon.facebookClicks);
            $('#general_view #coupon_counters tr td.couponBudget').text(e.coupon.maxBudget );
        }

        //$('#general_view #coupon_counters tr').each(function() {
           // var td = $(this).find('td:eq(1)');
            //TODO: fix 'views' is undefined adter pressing confirm button on end page
           // td.text('').text(e.coupon[td.data('key')]);
        //});
    }, {}, true);

    // Bind coupon editing button
    $("#general_view #updateCoupon").on('click', function () {
        Navigation.changePage('edit_coupon', {id: couponId});
    });

    // Bind coupon pausing button
    $("#general_view #pauseCoupon").on('click', function () {
        API._put('/coupons/' + couponId + '/pause', function(e) {
            Navigation.changePage('coupon', {id: couponId});
        });
    });

    $("#general_view #deleteCoupon").on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        Navigation.confirm(Language.scheme.alerts.are_you_sure_delete_coupon, function() {
            API._delete('/coupons/' + couponId, function(data) {
                if (!data.isCouponDelete) {
                    Navigation.alert(Language.scheme.alerts.something_wrong_try_again);
                }

                Navigation.changePage('coupons');
            });
        });
    });

    var feedbacksCount = 0;
    var positiveFeedbacks = 0;
    var negativeFeedbacks = 0;

    API._get('/comments', function(data) {
        if (typeof data['couponComments'] == 'undefined') {
            return false;
        }

        feedbacksCount = data.couponComments.length;

        if (feedbacksCount > 0) {
            for (var i in data.couponComments) {
                var comment = data.couponComments[i];

                if (comment.likes == 3) {
                    positiveFeedbacks++;
                } else if (comment.likes == 1) {
                    negativeFeedbacks++;
                }
            }
        }

        $('#general_view #coupon_counters tr td.numberOfFeedbacks').text(feedbacksCount);
        $('#general_view #coupon_counters tr td.numberOfPositiveReview').text(positiveFeedbacks);
        $('#general_view #coupon_counters tr td.numberOfNegativeReview').text(negativeFeedbacks);
    }, {couponId: couponId, userId: false}, false);
};

Navigation.controllers['user_coupon'] = function(data) {
    if (typeof data == 'undefined') {
        Navigation.changePage('user_main');
        return false;
    }
    var id = data.id;

    API._get('/coupons/' + data.id, function(data) {
        if (typeof data.coupon == 'undefined') {
            Navigation.changePage('user_main');
            return false;
        }

        var c = data.coupon;

        console.log('c', c);

        $('#general_view .benefit-business-title span').text(c.couponName);
        $('#general_view .benefit-business-title img').attr({
            src: c.logo,
            width: 60,
            height: 60,
            alt: c.businessName
        });

        $('#general_view #business_name').text(c.businessName);
        $('#general_view .benefit-image img').attr('alt', c.couponName);
        $('#general_view .benefit-image img').attr('src', c.couponImage);
        $('#general_view .benefit-image .coupon_name').text(c.couponName);

        $('#general_view .benefit-tag .new_price').text(c.tariff);
        $('#general_view .benefit-tag .old_price').text(c.cellPrice);

        //TODO: Я хер его знает для чего это писалось, но из-за этой функции валится цены в НаН
        //buildLocalPrices();

        $('#general_view .benefit-business-about .description').text(c.couponDescription);
        $('#general_view .benefit-description').text(c.MarketingStatement);

        console.log('Language.language', Language.language);
        $('#general_view .benefit-valid .valid_date').text(moment(c.dateTo).format( (Language.language == 'he') ? 'D.M.YYYY, H:mm' : 'D.M.YYYY, H:mm a' ));

        $('#general_view .benefit-actions-call a').attr('href', 'tel:' + c.contactPhone);
        $('#general_view .benefit-actions-call a').on('click', function() {
            API._put('/coupons/' + id + '/countCalls');
        });

        $('#general_view .coupon_number').text(c.couponNumber);

        var readableWorkTime = '';

        readableWorkTime = c.workTime.workdays + ' from ' + c.workTime.workdaysHoursFrom + ' to ' + c.workTime.workdaysHoursTo;
        if (c.workTime.friday) {
            readableWorkTime + '<br /> Friday from ' + c.workTime.fridayHoursFrom + ' to ' + c.workTime.fridayHoursTo;
        }
        $('#general_view .benefit-business-hours p').text(readableWorkTime);

        $('#general_view .little-letters').text(c.smallLetters);

        $('#general_view .benefit-business-icons-map').attr('href', 'http://maps.google.com/?q=' + c.loc[1] + ',' + c.loc[0]);

        $('#general_view .benefit-business-icons-home').attr('href', c.linkToProduct);
        $('#general_view .benefit-business-icons-home').on('click', function() {
            API._put('/coupons/' + id + '/countSiteClicks');
        });

        $('#general_view .facebook-share').on('click', function(e) {
            e.preventDefault();
            API._put('/coupons/' + id + '/countFacebookClicks');

            var url = 'http://coupit.co.il/web/single.htm?id='+id;
            var text = 'וואו מצאתי הטבה של "' + c.couponName+ '" משהו פגז שווה להעיף מבט';
            console.log('url',url);
            console.log('text',text);

            window.plugins.socialsharing.shareViaFacebook(text, null /* img */, url,
                function() {
                    console.log('Facebook shared successfully');
                },
                function(error) {
                    console.error(error);
                }
            );
        });

        // Comments
        API._get('/comments', function(data) {
            if (typeof data['couponComments'] != 'undefined') {
                $('#general_view .comments_count').text(data.couponComments.length);
            } else {
                $('#general_view .comments_count').text(data.userCommentsByCoupon.length);
            }
        }, {couponId: id, userId: false});

        $('#general_view .coupon_comments').on('click', function(e) {
            e.preventDefault();
            Navigation.changePage('comments', {id: id});
        });

        var button = $('#general_view .benefit-actions-save a');

        API._get('/coupons/currUser',
            function(data) {
                if (data.couponsByUser.length > 0) {
                    for (var i in data.couponsByUser) {
                        var coupon = data.couponsByUser[i];

                        if (coupon._id == c._id) {
                            button.addClass('clicked');
                            button.find('span').text(Language.scheme.messages.use_it);
                        }
                    }
                }
            }
        );

        if (!button.hasClass('clicked')) {

            API._get('/coupons/currUser/archive',
                function(data) {
                    if (data.archiveCouponsByUser.length > 0) {
                        for (var i in data.archiveCouponsByUser) {
                            var coupon = data.archiveCouponsByUser[i];

                            if (coupon._id == c._id) {
                                button.removeClass('clicked').addClass('archived');
                                button.find('span').text(Language.scheme.messages.archived);
                            }
                        }
                    }
                }
            );
        }

        button.on('click', function(e) {
            e.preventDefault();

            if (button.hasClass('archived')) {
                return false;
            }

            if (button.hasClass('clicked')) {

                API._post('/coupons/currUser/archive', function(data) {

                    button.removeClass('clicked').addClass('archived');
                    button.find('span').text(Language.scheme.messages.archived);

                    Navigation.alert_continue(Language.scheme.alerts.thank_you_for_using_benefit, function() {
                        Navigation.changePage('user_coupons');
                    });
                }, {couponId: c._id});
            } else {
                API._post('/coupons/currUser/favourite', function(data) {

                    if (data.isCouponFavourite == true) {
                        var couponDateTo = moment(c.dateTo.substr(0, 10).split('-'));
                        var today = new Date();
                        var currentDate = moment([today.getFullYear(), today.getMonth()+1, today.getDate()]);
                        var daysToExpire = couponDateTo.diff(currentDate, 'days') + 1;

                        Navigation.alert(Language.scheme.messages.use_it_before_alert + daysToExpire + Language.scheme.messages.use_it_after_alert);
                        button.addClass('clicked');
                        button.find('span').text(Language.scheme.messages.use_it);

                    } else {
                        button.removeClass('clicked');
                        button.find('span').text(Language.scheme.messages.give_it_to_me);
                    }
                }, {couponId: c._id});
            }
        });
    });
};