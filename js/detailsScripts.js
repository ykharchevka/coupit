Navigation.controllers['details'] = function (data) {
    var couponId = undefined;
    var detailsCouponsId = [];
    var couponIndex = 0;
    if (data && data.id) {
        couponId = data.id;
    }
    
    API._get('/coupons/' + couponId, function (e) {
        if (e.coupon) {
            $("#general_view .couponNumber").text(e.coupon.couponNumber);
            $("#general_view .couponDescription").text(e.coupon.couponDescription);
            $("#general_view .dateTo").text(dateFormat(e.coupon.dateTo));
            $("#general_view .creditBalance").text(e.coupon.maxBudget);
            $("#general_view .couponImage").attr("src", e.coupon.couponImage);

            $('#general_view tr td.numberOfViews').text(e.coupon.views);
            $('#general_view tr td.numberOfClicks').text(e.coupon.clicks);
            $('#general_view tr td.numberOfShares').text(e.coupon.shares);
            $('#general_view tr td.numberOfCalls').text(e.coupon.calls);
            $('#general_view tr td.numberOfSiteClicks').text(e.coupon.siteClicks);
            $('#general_view tr td.numberOfFacebookClicks').text(e.coupon.facebookClicks);
            $('#general_view tr td.couponBudget').text(e.coupon.maxBudget);
        }
    }, {}, true);

    var feedbacksCount = 0;
    var positiveFeedbacks = 0;
    var negativeFeedbacks = 0;

    API._get('/comments', function (data) {
        if (typeof data['couponComments'] == 'undefined') {
            return false;
        }

        feedbacksCount = data.couponComments.length;

        if (feedbacksCount > 0) {
            for (var i in data.couponComments) {
                var comment = data.couponComments[i];

                if (comment.likes == 3) {
                    positiveFeedbacks++;
                } else if (comment.likes == 1) {
                    negativeFeedbacks++;
                }
            }
        }


        $('#general_view tr td.numberOfFeedbacks').text(feedbacksCount);
        $('#general_view tr td.numberOfPositiveReview').text(positiveFeedbacks);
        $('#general_view tr td.numberOfNegativeReview').text(negativeFeedbacks);
    }, { couponId: couponId, userId: false }, false);

    API._get('/coupons/client', function (data) {
        if (data.couponsByClient && data.couponsByClient.length > 0) {
            
            for (var i = 0; i < data.couponsByClient.length; i++) {
                var c = data.couponsByClient[i];
                if (typeof c.payment != 'undefined') {
                    detailsCouponsId.push(c._id);
                }
            }
            couponIndex = detailsCouponsId.indexOf(couponId);
            if (couponIndex == 0) {
                $("#general_view .prevCoupon").attr('disabled', true);
            } else if (couponIndex == detailsCouponsId.length - 1) {
                $("#general_view .nextCoupon").attr('disabled', true);
            }
        }
    });

    $("#general_view .prevCoupon").on('click', function (e) {
        var curId = detailsCouponsId[couponIndex - 1];
        Navigation.changePage('details', { id: curId });
    });

    $("#general_view .nextCoupon").on('click', function (e) {
        var curId = detailsCouponsId[couponIndex + 1];
        Navigation.changePage('details', { id: curId });
    });
};