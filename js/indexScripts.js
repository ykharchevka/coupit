﻿Navigation.controllers['home'] = function() {

    /*** FACEBOOK REGISTER: BEGIN ***/

    $('#btFacebook').on('click', function(e){
        if ( !$("#check1").is(":checked") ) {
            Navigation.alert(Language.scheme.alerts.you_must_agree);
            return false;
        }

        var userType = 'USER';

        fbLogin(userType);
    });

	$('.fbHandler').on('click', function(e,userType) {
		fbLogin(userType);
	});

    var FACEBOOK_API_KEY = '344667469071143';
    var REDIRECT_URL = window.config.hostName + '/oauthcallback.html';

    function fbLogin(userType) {
        openFB.init(FACEBOOK_API_KEY, REDIRECT_URL, window.localStorage);
        openFB.login('email', fbGetInfo.bind(this, userType), Navigation.alert('Facebook login failed'));
    }

    function fbRegister(data,userType){
        console.log('fbRegister/userType',userType);
        var userData = {
            id : data.id,
            email : data.email,
            firstName: data.first_name,
            gender : data.gender,
            lastName : data.last_name,
            link : data.link,
            locale: data.locale,
            name : data.name,
            timezone : data.timezone,
            updatedTime : data.updated_time,
            verified: data.verified,
            facebook: true
        };

        API._post('/sessions', function(e, statusCode) {
            localStorage.login = true;
            localStorage.user = JSON.stringify(e.user);
            localStorage.userType = e.user.role;

            if( !localStorage.getItem('firstFacebookLogin') ) {
                localStorage.setItem('firstFacebookLogin', true);
            }

            if (userType === 'CUSTOMER') {
                Navigation.changePage('settings');
            } else {
                Navigation.changePage('user_main');
            }
        }, userData);
    }

    function fbGetInfo(userType) {
        openFB.api({
            path: '/me',
            success: function(data) {
                fbRegister(data,userType)
            },
            error: fbErrorHandler
        });
    }

    function sbShare() {
        openFB.api({
            method: 'POST',
            path: '/me/feed',
            params: {
                message: 'Testing Facebook APIs'
            },
            success: function(data) {
                alert('the item was posted on Facebook');
            },
            error: fbErrorHandler});
    }

    function fbRevoke() {
        openFB.revokePermissions(
            function() {
                alert('Permissions revoked');
            },
            fbErrorHandler);
    }

    function fbErrorHandler(error) {
        alert(error.message);
    }

    /*** FACEBOOK REGISTER: END ***/

    var goToLogin = location.search.split('goToLogin=')[1];
    // if (localStorage.registered && goToLogin != 0) {
    //     Navigation.changePage('login');
    // }

    $("#general_view #skipButton").click(function () {
        Navigation.changePage('mainSkip');
    });


    $("#general_view #btRegister, #general_view #btFacebook, #general_view .homebar a[href='#registration']").click(function (e) {
        if (!$("#general_view #check1").is(":checked")) {
            Navigation.alert(Language.scheme.alerts.you_must_agree);
            e.stopImmediatePropagation();
            return false;
        }
    });

    $('#skip_to_user').on('click', function() {
      API._post('/users', function(e) {
        if (e.idExists) {

        }

        API._post('/sessions', function(f) {
            localStorage.login = true;
            localStorage.user = JSON.stringify(f.user);
            localStorage.userType = f.user.role;

            if (localStorage.userType == 'client') {
                Navigation.changePage('main');
            } else {
                Navigation.changePage('user_main');
            }
        }, {id: window.deviceId});

    }, {id: window.deviceId});
    });
};
