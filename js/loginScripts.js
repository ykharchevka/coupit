﻿Navigation.controllers['login'] = function() {

    $('#email').on('blur', function(){
        if(!validateEmail($(this).val()) && $(this).val().length > 0) {
            Navigation.alert(Language.scheme.alerts.username_is_wrong);
            $(this).val('');
            return false;
        }
    });

    $('#pass').on('blur', function() {
        if($(this).val() == 0) {
            Navigation.alert(Language.scheme.alerts.password_is_empty);
            $(this).val('');
            return false;
        }
    });


    $('#general_view #login').on('click', function (e) {
        e.stopImmediatePropagation();

        if(!validateEmail( $('#general_view #email').val() ) ) {
            Navigation.alert(Language.scheme.alerts.username_is_wrong);
            $(this).val('');
            return false;
        }

        if($('#general_view #pass').val().length == 0) {
            Navigation.alert(Language.scheme.alerts.password_is_empty);
            $(this).val('');
            return false;
        }

        if ($('#general_view #email').val()) {
            API._post('/sessions', function(e, statusCode) {
                if (e.notFound || statusCode == 400) {
                    localStorage.login = false;
                    Navigation.alert(Language.scheme.alerts.username_or_password_is_wrong);
                }
                else {
                    localStorage.login = true;
                    localStorage.user = JSON.stringify(e.user);
                    localStorage.userType = e.user.role;

                    if (localStorage.userType == 'user') {
                        Navigation.changePage('user_main');
                    } else {
                        Navigation.changePage('main');
                    }
                }
            }, {email: $('#general_view #email').val(), password: $('#general_view #pass').val()});
        }
        else {
            Navigation.alert(Language.scheme.alerts.login_empty);
        }
    });
};