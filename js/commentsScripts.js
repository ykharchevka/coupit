Navigation.controllers['user_comments'] = function() {
	$('#general_view .my-comments table tbody').html('');

	API._get('/comments', function(data) {
		var couponsIds = [];

		if (data.userComments.length == 0) {
			$('#general_view .my-comments').children().remove();
			$('#general_view .my-comments').append('<h2 data-t="no_comments"></h2>');
			Language.translate();

			return false;
		}

		for (var i in data.userComments) {
			couponsIds.push(data.userComments[i].coupon);
		}

		var coupons;
		API._get('/coupons/' + couponsIds.join(','), function(data) {
			coupons = data.coupon;
		}, {}, false);

		for (var i in data.userComments) {
			var comment = data.userComments[i];

			var coupon = {};

			for (var j in coupons) {
				if (coupons[j]._id == comment.coupon) {
					coupon['couponNumber'] = coupons[j].couponNumber;
					coupon['storeName'] = coupons[j].storeName;
				}
			}


			var mood = '';
			switch(comment.likes) {
				case 1:
					mood = 'sad';
					break;
				case 2:
					mood = 'calm';
					break;
				case 3:
					mood = 'happy';
					break;
			}

			var tr = $(
				'<tr data-id="' + comment._id + '">' +
				'<td><i class="' + mood + '">A</i></td>' +
				'<td>' + coupon.couponNumber + '</td>' +
				'<td>' + coupon.storeName + '</td>' +
				'</tr>' +
				'<tr data-id="' + comment._id + '">' + 
				'<td colspan="3">' +
					'<p>' + comment.body + '</p>' +
					'<a class="remove-comment pop" href="#remove"><i>x</i><span data-t="delete_comment">מחק תגובה</span></a>' +
				'</td>' +
				'</tr>'
			);

			$('#general_view .my-comments table tbody').append(tr);
		}

		Language.translate();
		bindMfpPopup();

		$('#general_view .remove-comment').on('click', function(e) {
			var id = $(this).parents('tr').data('id');
			$('#general_view .my-comments').data('comment_id', id);
		});

		$('#general_view .confirm').on('click', function(e) {
			var id = $('#general_view .my-comments').data('comment_id');

			API._delete('/comments/' + id, function() {
				$('#general_view .my-comments tr[data-id="' + id + '"]').remove();
			});

			$.magnificPopup.instance.close();
			// e.preventDefault();
		});

		$('#general_view .cancel').on('click', function() {
			$.magnificPopup.instance.close();
		});		
	});
}

Navigation.controllers['comments'] = function(data) {
	$('#general_view').on('click', '.comments-list div', function(e) {
		e.stopImmediatePropagation();

		$(this).toggleClass('open');
	});

	function buildCommentElement(likes, body) {
		var mood = '';

		switch (likes) {
			case 1:
				mood = 'sad';
				break;
			case 2:
				mood = 'calm';
				break;
			case 3:
				mood = 'happy';
				break;
		}

		if (mood != '') {
			mood = 'class="' + mood + '"';
		}

		return $(
			'<div ' + mood + '>' +
			'<p>' + body + '</p>' +
			'</div>'
		);
	}

	if (typeof data == 'undefined') {
		Navigation.changePage('home');
		return false;
	}

	var id = data.id;

	$('#general_view .comments-list').html('');

	API._get('/comments', function(data) {
		$('#general_view .comments_count').text(data.userCommentsByCoupon.length);

		for (var i in data.userCommentsByCoupon) {
			var c = data.userCommentsByCoupon[i];
			
			$('#general_view .comments-list').append(buildCommentElement(c.likes, c.body));
		}

		
	}, {couponId: id, userId: false});

	$('#general_view .add_comment_form').on('submit', function(e) {
		e.preventDefault();

		var likes = $(this).find('input[name="mood"]:checked').val();
		if (!likes) {
			likes = 0;
		}

		var body = $(this).find('textarea').val();

		if (body.length == 0) {
			alert('oh nooo');
			return false;
		}

		var userId = JSON.parse(localStorage.user)._id;
		var form = this;

		API._post('/comments', function(data) {
			if (data.comment) {
				$('#general_view .comments-list').prepend(buildCommentElement(data.comment.likes, data.comment.body));

				$('#general_view .comments_count').text(parseInt($('#general_view .comments_count').text()) + 1);

				$(form).find('textarea').val('');
				$(form).find('input[name="mood"]:checked').removeAttr('checked');
			}
		}, {likes: likes, body: body, couponId: id, userId: userId});
	});

	Navigation.goBack = function() {
		Navigation.changePage('user_coupon', {id: id});
	}
}