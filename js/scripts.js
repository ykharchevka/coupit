window.showWarningAlertOnLeave = false;
var deviceId = '';
var currencies = ['UAH_USD', 'USD_UAH', 'ILS_USD', 'USD_ILS', 'UAH_ILS', 'ILS_UAH'];
var currencyRates = {};

$(document).on('click', function(){
	if(Language.language == 'he'){
		$('link[title="hebrew"]').removeAttr('disabled');
	} else {
		$('link[title="hebrew"]').prop('disabled', true);
	}
});

window.onerror = function(errorMsg, url, lineNumber) {
    if (window.config.environment === 'DEVELOPMENT') {
	   alert('Error occured: ' + errorMsg + ', line: ' + lineNumber + ', url: ' + url);
    }
};

function numbersonly(myfield, e, size) {
    if (!size)
        size = 4;
	var key;
	var keychar;

	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which;
	else
		return true;

	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
		return true;

	// numbers
	else if ((("0123456789").indexOf(keychar) > -1 && myfield.value.length <= size-1)) {
		return true;
	}

	// only one decimal point
	else if ((keychar == "."))
	{
		if (myfield.value.indexOf(keychar) > -1 ) {
			return false;
		}
	}
	else {
		return false;
	}
}

function dateFormat(date) {
    var fullDate = new Date(date);
    var day = fullDate.getDate();
    var month = fullDate.getMonth();
    var year = fullDate.getFullYear();

    var correctDate = day + "/" + (month + 1) + "/" + year;

    return correctDate;
}

function htmlDateInputFormat(date) {
    var fullDate = new Date(date);
    var day = fullDate.getDate();
    if (day < 10)
        day = "0" + day;
    var month = fullDate.getMonth();
    if (month < 9)
        month = "0" + (month + 1);
    var year = fullDate.getFullYear();


    var correctDate = year + "-" + month + "-" + day;

    return correctDate;
}

//regular expression for email
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function checkSpecialEmptiness() {
	var args = [].slice.apply(arguments);
	console.log(args);

	if (args.join('') == '') {
		return true;
	} else {
		for (var i in args) {
			if (!args[i]) {
				return false;
			}
		}

		return true;
	}
}

var populateCurrencyRates = function(data) {
	for (var i in data) {
		currencyRates[i] = parseFloat(data[i].val);
	}
}

var serializedToObject = function(data) {
	var result = {};

	for (var i in data) {
		result[data[i].name] = data[i].value;
	}

	return result;
}

deviceReady = function() {
	document.addEventListener("menubutton", function() {
		console.log('addEventListener("menubutton"')
		window.location = '#settings';
	}, false);

	if (window.location.search.indexOf('paypalRedirect=true') != -1) {
		var orderId = '';

		var paramGroups = window.location.search.split('&');
		for (var i in paramGroups) {
			var paramArr = paramGroups[i].split('=');
			if (paramArr[0] == 'orderId') {
				localStorage.orderId = paramArr[1];
				break;
			}
		}


		window.location = '/#charge';
		return false;
	}

	if (Navigation.curPage == '' && window.location.hash != '') {
		jQuery('body').data('default_page', window.location.hash.substr(1));
	}

	deviceId =  window.device ? window.device.uuid : '';

	Language.init();
	Navigation.init();
	Language.translate();

	$('#sliding_menu .back').on('click', function(e) {
		e.preventDefault();

		var unreturnablePages = ['home', 'main', 'user_main'];

		if (unreturnablePages.indexOf(Navigation.curPage) === -1) {
			history.back();
		}
	});

	$('a.logout').on('click', function() {
		API._delete('/sessions', function() {
			delete localStorage.login;
	    	delete localStorage.user;
	    	delete localStorage.userType;

	    	Navigation.changePage('home');
		});
	});
}

var app = /^file:\/{3}[^\/]/i.test(window.location.href) && /ios|iphone|ipod|ipad|android/i.test(navigator.userAgent);

if (app) {
	document.addEventListener("deviceready", deviceReady, false);
} else {
	$(deviceReady);
}

Navigation.controllers['general'] = function(){
	if (navigator.userAgent.indexOf('Android') !== -1) {
		$('body').addClass('android');
	}

	$(".main").delay(1000).fadeIn("slow");
	$('table tr:even, .comments-list div:even').addClass('even');

	$('.settings-list h3').each(function(){
		$(this).click(function () {
		if ($(this).next('form').is(":hidden")) {
			$(this).next('form').slideDown("fast")
		}
		else {
			$(this).next('form').slideUp("fast")
		}
		})
	});
	$('#general_view .home-check label').each(function(){
		$(this).click(function () {
			if ($('#general_view .terms-close').is(":hidden")) {
				$('#general_view .terms-close').slideDown("fast");
				$('#general_view .terms').slideDown("fast");
			} else {
				$('#general_view .terms-close').slideUp("fast");
				$('#general_view .terms').slideUp("fast");
			}
		});
	});

	$('#general_view .terms-close').each(function(){
		$(this).click(function () {
			if ($('#general_view .terms-close').is(":hidden")) {
				$('#general_view .terms').slideDown("fast");
				$(this).slideDown("fast");
			} else {
				$('#general_view .terms').slideUp("fast");
				$(this).slideUp("fast");
			}
		});
	});

	$('#general_view .near-top-edit>a,#general_view .near-top-edit form>i').click(function () {
		if ($("#general_view .near-top-edit form").is(":hidden")) {
			$("#general_view .near-top-edit form").slideDown()
		}
		else {
			$("#general_view .near-top-edit form").slideUp()
		}
	});

	$('.mycoupon-menu-delay,.mycoupon-delay-confirm i').each(function(){
		$(this).click(function () {
		if ($('.mycoupon-delay-confirm').is(":hidden")) {
			$('.mycoupon-delay-confirm').slideDown("fast");
		}
		else {
			$('.mycoupon-delay-confirm').slideUp("fast");
		}
		})
	});

	$('.checks.cats :checkbox').change(function () {
		var checkedCheckBoxes = $('.checks.cats').find(':checkbox:checked');
		if (checkedCheckBoxes.length > 2) {
			this.checked = false;
			$(".checks-error").slideDown();
		}
		else {
			$(".checks-error").slideUp();
		}
	});

	$('.checks-error i').click(function(){
		$('.checks-error').slideUp();
	});

	$('.hascustom').each(function(){
		$(this).change(function(){
	        if( $(this).find('option:selected').val() == 'custom' ) {
	            $(this).parent('fieldset').find('input').show();
	        }
	        else{
	        	$(this).parent('fieldset').find('input').hide();
	        }
   		});
	});

	bindMfpPopup();

	$.magnificPopup.instance.close();

	$('.mfp_close').on('click', function() {
		$.magnificPopup.instance.close();
	});


    $('a.menu').unbind();
	$('a.menu').on('click', function (e){
		e.preventDefault();
        e.stopPropagation();

        if(localStorage.userType == 'user') {
            console.log('user')
            Navigation.changePage('user_settings');
        } else if(localStorage.userType == 'client') {
            console.log('client')
            Navigation.changePage('settings');
        } else {
            console.log('unautorizedSettings')
            Navigation.changePage('unautorizedSettings');
        }
	});

    if (localStorage.userType) {
    	$('body').attr('role', localStorage.userType);
    } else {
    	$('body').attr('role', 'guest');
    }

    fontSizer.resize();
};

function bindMfpPopup() {
	if ($.isFunction($.fn.magnificPopup)) {
    	$('#general_view .pop').magnificPopup({type:'inline'});
    	$('#general_view .cancel').click(function(){
    		$.magnificPopup.close();
    	})
		$('#general_view .create').magnificPopup({ items: {src: '#create'},type: 'inline'}, 0);
		$('#general_view .ok').magnificPopup({ items: {src: '#ok'},type: 'inline'}, 0);
	};
}

function equalHeight(group) {
	tallest = 0;
	group.each(function() {
		thisHeight = $(this).height();
		if(thisHeight > tallest) {
			tallest = thisHeight;
		}
	});
	group.height(tallest);
}

function buildLocalPrices() {
	//return;
	var localPricesInterval = setInterval(function() {
		if (Object.getOwnPropertyNames(currencyRates).length == currencies.length) {
			console.log('ok!');
			clearInterval(localPricesInterval);

			var currency, price = {};
			var currency_pos, price_pos = 0;

			$('#general_view .price_elm').each(function() {
				if ($(this).children('span:eq(0)').text() == parseFloat($(this).children('span:eq(0)').text())) {
					currency_pos = 1;
					price_pos = 0;
				} else {
					currency_pos = 0;
					price_pos = 1;
				}

				price = {p: price_pos, value: parseFloat($(this).children('span:eq(' + price_pos + ')').text())};
				currency = {p: currency_pos, value: $(this).children('span:eq(' + currency_pos + ')').text()};

				var currency_from = '';
				var currency_to = Language.scheme.config.currency_code;
				var result_currency = Language.scheme.messages.currency;

				for (var i in LanguageScheme) {
					if (LanguageScheme[i].messages.currency == currency.value) {
						currency_from = LanguageScheme[i].config.currency_code;
					}
				}

				var result_price = (currencyRates[currency_from + '_' + currency_to] * price.value).toFixed(2);

				$(this).children('span:eq(' + currency.p + ')').text(result_currency);
				$(this).children('span:eq(' + price.p + ')').text(result_price);

				$(this).attr('title', currency.value + price.value);

			});
		}
	}, 200);
}