﻿var bindCouponButtons = function() {
    $('#general_view .show_coupon').on('click', function(e) {
        e.preventDefault();

        var id = $(this).parents('.coupon').data('id');
        console.log(id);
        Navigation.changePage('user_coupon', {id: id});
    });

    $('#general_view .delete_coupon').on('click', function(e) {
        e.preventDefault();

        var id = $(this).parents('.coupon').data('id');
        $('#general_view .delete_coupon_popup').data('coupon_id', id);
    });
}
var userCouponsController = function(suffix) {
    // Populate categories dropdown list
    $('#noBenefits').remove();
    API._get('/coupons/categories?lang=' + Language.language, function(data) {
        $('#general_view .near-top-edit form').html('');
        $('#general_view .filter-icons').html('');

        for (var i in data.categories) {
            var c = data.categories[i];

            $('#general_view .near-top-edit form').append('<fieldset><input checked="checked" id="near' + c._id + '" data-cat_id="' + c._id + '" type="checkbox" /><label for="near' + c._id + '">' + c.name + '</label></fieldset>');
            $('#general_view .filter-icons').append('<a href="javascript:void(0);" data-id="' + c._id + '"><i>' + c.icon + '</i><span>' + c.name + '</span></a>')
        }

        $('#general_view .filter-icons a').on('click', function(e) {
            var id = $(this).data('id');
            if (Navigation.curPage == 'user_coupons') {
                window.sessionStorage.setItem("user_only", '1');
            } else {
                window.sessionStorage.removeItem("user_only");
            }
            Navigation.changePage('category', {id: id});
        });
    }, {}, false);


    $('#general_view .mycoupons').html('');

    var url = '/coupons/currUser';

    if (typeof suffix !== 'undefined') {
        url += suffix;
    }

    API._get(url,
        function(categories) {
            $('#general_view .near-top-update .refreshed_time').text(moment().format('HH:mm'));
            $('#noBenefits').remove();
            var totalCoupons = 0;
            var goodCoupons = 0;
            var goodCats = 0;
            var cats = false;
            if (typeof categories['expireCoupons'] != 'undefined') {
                cats = categories['expireCoupons'];
            } else {
                cats = categories['couponsByUser'];
            }

            for (var cat in cats) {
                totalCoupons += cats[cat].coupons.length;
            }

            if (totalCoupons == 0) {
              //  $('#general_view .mycoupons').append('<h2 data-t="no_coupons"></h2>');
            } else {
                for (var cat in cats) {
                    if (cats[cat].coupons.length > 0) {
                        $('#general_view .mycoupons').append('<h3 data-cat_id="' + cats[cat]._id + '" class="category-header"><i>'+cats[cat].icon+'</i><span>' + cats[cat].name + '</span></h3>');

                        for (var c in cats[cat].coupons) {
                            var coupon = cats[cat].coupons[c];
                            if (moment(coupon.dateTo).unix() < moment().startOf('day').unix()) {
                                continue;
                            }
                            if (coupon.status == "onAir") {
                                goodCoupons++;
                                $('#general_view .mycoupons').append(
                                    '<div class="c coupon" data-id="' + coupon._id + '">' +
                                    '<a class="pop x-button delete_coupon" href="#delete"><i>x</i><span data-t="delete_coupon">מחק הטבה</span></a>' +
                                    '<h2>' + coupon.couponName + '</h2>' +
                                    '<div class="c">' +
                                        '<div class="half2"><img alt="" width="" height="" src="' + coupon.couponImage + '"></div>' +
                                        '<div class="half2">' +
                                            '<div class="data-table">' +
                                                '<table>' +
                                                    '<tr>' +
                                                        '<td data-t="c_business">בית העסק</td>' +
                                                        '<td>' + coupon.storeName + '</td>' +
                                                    '</tr>' +
                                                    '<tr>' +
                                                        '<td data-t="description">תיאור</td>' +
                                                        '<td>' + coupon.couponDescription + '</td>' +
                                                    '</tr>' +
                                                    '<tr>' +
                                                        '<td data-t="c_price">מחיר</td>' +
                                                        '<td class="eryeyey">' + '<span class="price_elm">' +
                                                            '<span>' + coupon.cellPrice + '</span> <span>₪</span></span> ' +
                                                            '<span class="price_elm"><span>' + coupon.tariff + '</span> <span>₪</span></span></td>' +
                                                    '</tr>' +
                                                    '<tr>' +
                                                        '<td data-t="c_expiring_at">תוקף</td>' +
                                                        '<td>' + moment(coupon.dateTo).format((Language.language == 'he') ? 'D.M.YYYY, H:mm' : 'D.M.YYYY, H:mm a') + '</td>' +
                                                    '</tr>' +
                                                    '<tr>' +
                                                        '<td data-t="number">מספר</td>' +
                                                        '<td>' + coupon.couponNumber + '</td>' +
                                                    '</tr>' +
                                                '</table>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                    '<div class="mycoupons-nav">' +
                                        '<a href="#" class="show_coupon" data-t="show_business">הצג לבית העסק</a>' +
                                        '<div class="mycoupons-nav-icons">' +
                                            '<a target="_blank" href="tel:' + coupon.contactPhone + '"><i>q</i><span data-t="call">חייג</span></a>' +
                                            '<a class="pop" href="#share"><i>k</i><span data-t="share">שתף</span></a>' +
                                            '<a target="_blank" href="http://maps.google.com/?q=' + coupon.loc[1] + ',' + coupon.loc[0] + '"><i>w</i><span data-t="map">מפה</span></a>' +
                                        '</div>' +
                                        '<div class="mfp-hide white-popup" id="share">' +
                                            '<div class="white-popup-share">' +
                                                '<div class="c">' +
                                                    '<a target="_blank" href="http://www.facebook.com/sharer.php?m2w&s=100&p[url]=<?php echo current_page_url(); ?>"><i>a</i></a>' +
                                                    '<a href="mailto:?subject=שווה להעיף מבט&amp;body=שולח לך הטבה מצוינת שעניינה אותי. שווה להעיף מבט http://www.website.com."><i>Q</i></a>' +
                                                '</div>' +
                                                '<div class="c">' +
                                                    '<a href="whatsapp://send?text=http://www.mydomain.com"><i>R</i></a>' +
                                                    '<a href="sms:?body=test"><i>S</i></a>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>');
                            }
                        }
                        if (goodCoupons == 0) {
                            $('#general_view .mycoupons h3[data-cat_id=' + cats[cat]._id + ']').hide();
                        } else {
                            goodCoupons = 0;
                            goodCats++;
                        }
                    }
                }
                $('#noBenefits').remove();
                if (goodCats == 0) {

                    $('<div id="noBenefits">' + Language.scheme.messages.no_coupons + '</div>').prependTo($('.mycoupons.usermycoupons.c').parent());
                }
            }

            $('.delete_coupon_popup .confirm').on('click', function(e) {
                e.preventDefault();

                var id = $(this).parents('.delete_coupon_popup').data('coupon_id');
                API._post('/coupons/currUser/favourite', function(data) {
                    if (typeof data['isCouponInArchive'] != 'undefined') {
                        API._delete('/coupons/currUser/archive/' + id, function(){}, {}, false);
                    }

                    $('#general_view .coupon[data-id="' + id + '"]').remove();
                    $.magnificPopup.close();
                }, {couponId: id});
            });

            Language.translate();

            bindMfpPopup();


            $('#general_view .near-top-edit input[type="checkbox"]').on('change', function(e){
                console.log('change')
                var id = $(this).data('cat_id');
                var header = $('#general_view h3[data-cat_id="' + id + '"]');
                var coupons = header.nextUntil('h3');

                if ($(this).is(':checked')) {
                    header.show();
                    coupons.show();
                } else {
                    header.hide();
                    coupons.hide();
                }

                var hiddenRows = 0;
                $(".c.coupon").each(function () {
                    if ($(this).css('display') == 'none') {
                        hiddenRows++;
                    }
                });

                $('#noBenefits').remove();
                if ($('c.coupon').length == hiddenRows) {
                    $('<div id="noBenefits">' + Language.scheme.messages.no_coupons + '</div>').prependTo($('.mycoupons.usermycoupons.c').parent());
                    //$('.mycoupons.usermycoupons.c').parent().append('<div id="noBenefits">' + Language.scheme.messages.no_coupons + '</div>');    #append after archive cat
                }

            });
        }
    , {categories: true}, false);
};

var appendArchivedCoupons = function() {
    API._get('/coupons/currUser/archive', function(data) {
        $('#general_view .mycoupons').append('<h3 class="category-header" id="archived"><i>B</i><span data-t="archived"></span></h3>');
        var archived_coupons = 0;
        for (var c in data.archiveCouponsByUser) {
            var coupon = data.archiveCouponsByUser[c];
            archived_coupons++;
            $('#general_view .mycoupons').append(
                '<div class="c coupon" data-id="' + coupon._id + '">' +
                '<a class="pop x-button delete_coupon" href="#delete"><i>x</i><span data-t="delete_coupon">מחק הטבה</span></a>' +
                '<h2>' + coupon.couponName + '</h2>' +
                '<div class="c">' +
                    '<div class="half">' +
                        '<img alt="" width="300" height="200" src="' + coupon.couponImage + '">' +
                    '</div>' +
                    '<div class="half">' +
                        '<div class="data-table">' +
                            '<table>' +
                                '<tr>' +
                                    '<td data-t="c_business">בית העסק</td>' +
                                    '<td>' + coupon.storeName + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                    '<td data-t="description">תיאור</td>' +
                                    '<td>' + coupon.couponDescription + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                    '<td data-t="c_price">מחיר</td>' +
                                    '<td><span class="price_elm"><span>' + coupon.cellPrice + '</span> <span>₪</span></span> <strike class="price_elm"><span>' + coupon.tariff + '</span> <span>₪</span></strike></td>' +
                                '</tr>' +
                                '<tr>' +
                                    '<td data-t="c_expiring_at">תוקף</td>' +
                                    '<td>' + moment(coupon.dateTo).format('D.M.YYYY, H:mm a') + '</td>' +
                                '</tr>' +
                                '<tr>' +
                                    '<td data-t="number">מספר</td>' +
                                    '<td>' + coupon.couponNumber + '</td>' +
                                '</tr>' +
                            '</table>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div class="mycoupons-nav">' +
                    '<a href="javascript:void(0);" class="show_coupon" data-t="show_business">הצג לבית העסק</a>' +
                    '<div class="mycoupons-nav-icons">' +
                        '<a target="_blank" href="tel:' + coupon.contactPhone + '"><i>q</i><span data-t="call">חייג</span></a>' +
                        '<a class="pop" href="#share"><i>k</i><span data-t="share">שתף</span></a>' +
                        '<a target="_blank" href="http://maps.google.com/?q=' + coupon.loc[1] + ',' + coupon.loc[0] + '"><i>w</i><span data-t="map">מפה</span></a>' +
                    '</div>' +
                    '<div class="mfp-hide white-popup" id="share">' +
                        '<div class="white-popup-share">' +
                            '<div class="c">' +
                                '<a target="_blank" href="http://www.facebook.com/sharer.php?m2w&s=100&p[url]=<?php echo current_page_url(); ?>"><i>a</i></a>' +
                                '<a href="mailto:?subject=שווה להעיף מבט&amp;body=שולח לך הטבה מצוינת שעניינה אותי. שווה להעיף מבט http://www.website.com."><i>Q</i></a>' +
                            '</div>' +
                            '<div class="c">' +
                                '<a href="whatsapp://send?text=http://www.mydomain.com"><i>R</i></a>' +
                                '<a href="sms:?body=test"><i>S</i></a>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>');
        }
        if (archived_coupons == 0)
            $("#archived").hide();
        Language.translate();
    }, {}, false);
};

Navigation.controllers['user_coupons'] = function() {
    $('.near-top-update').on('click', function() {
        Navigation.showLoader();
        userCouponsController()
        appendArchivedCoupons();
        bindCouponButtons();
        buildLocalPrices();
        Navigation.hideLoader();
    });

    userCouponsController();
    appendArchivedCoupons();
    bindCouponButtons();
    buildLocalPrices();
};

Navigation.controllers['user_expiring_coupons'] = function() {
    userCouponsController('/expire');
    bindCouponButtons();
    buildLocalPrices();
};

Navigation.controllers['all_coupons'] = function() {
    $('.near-top-update').on('click', function() {
        Navigation.showLoader();
        loadAllCoupons();
        buildLocalPrices();
        Navigation.hideLoader();
    });


    Navigation.showLoader();
    loadAllCoupons();
    buildLocalPrices();
    Navigation.hideLoader();

    //Navigation.hideLoader();


};


var loadAllCoupons = function() {
    // Populate categories dropdown list
    API._get('/coupons/categories?lang=' + Language.language, function(data) {
        $('#general_view .near-top-edit form').html('');
        $('#general_view .filter-icons').html('');

        for (var i in data.categories) {
            var c = data.categories[i];

            $('#general_view .near-top-edit form').append('<fieldset><input checked="checked" id="near' + c._id + '" data-cat_id="' + c._id + '" type="checkbox" /><label for="near' + c._id + '">' + c.name + '</label></fieldset>');
            $('#general_view .filter-icons').append('<a href="javascript:void(0);" data-id="' + c._id + '"><i>' + c.icon + '</i><span>' + c.name + '</span></a>')
        }

        $('#general_view .filter-icons a').on('click', function(e) {
            var id = $(this).data('id');
            if (Navigation.curPage == 'user_coupons') {
                window.sessionStorage.setItem("user_only", '1');
            } else {
                window.sessionStorage.removeItem("user_only");
            }
            Navigation.changePage('category', {id: id});
        });
    }, {}, true);

    $('#general_view .all_coupons').html('');

    var userCouponsIds = [];
    var archivedCouponsIds = [];

    API._get('/coupons/currUser', function (data) {
        if (data.couponsByUser.length > 0) {
            for (var i in data.couponsByUser) {
                userCouponsIds.push(data.couponsByUser[i]._id);
            }
        }
    }, {}, true);

    API._get('/coupons/currUser/archive', function (data) {
        if (data.archiveCouponsByUser.length > 0) {
            for (var i in data.archiveCouponsByUser) {
                archivedCouponsIds.push(data.archiveCouponsByUser[i]._id);
            }
        }
    }, {}, true);

    setTimeout(function () {
        API._get('/coupons?hideGeneral=true',
            function(categories) {
                $('#general_view .near-top-update .refreshed_time').text(moment().format('HH:mm'));

                for (var cat in categories.coupons) {
                    if (categories.coupons[cat].coupons.length > 0) {

                        var cat_block = $(
                            '<div class="near-item-row" data-cat_id="' + categories.coupons[cat]._id + '">' +
                            '<h3 class="category-header"><i>' + categories.coupons[cat].icon + '</i><span>' + categories.coupons[cat].name + '</span></h3>' +
                            '<div class="near-slider clearfix"></div></div>');

                        for (var c in categories.coupons[cat].coupons) {
                          var coupon = categories.coupons[cat].coupons[c];
                          if (moment(coupon.dateTo).unix() < moment(moment().format('YYYY-MM-DD')).unix()) {
                             continue;
                          }
                          if (coupon.status == "onAir") {
                                var saveButton = '<a class="near-item-save" date-to="' + coupon.dateTo + '"><i>v</i><span data-t="give_it_to_me">תן לי את זה</span></a>';
                                // If this coupon is already bookmarked - change "save" button style
                                if (userCouponsIds.indexOf(coupon._id) != -1) {
                                    saveButton = '<a class="near-item-save saved" date-to="' + coupon.dateTo + '"><i>v</i><span data-t="use_it">תן לי את זה</span></a>';
                                }

                                if (archivedCouponsIds.indexOf(coupon._id) != -1) {
                                    saveButton = '<a class="near-item-save archived" date-to="' + coupon.dateTo + '"><i>v</i><span data-t="archived"></span></a>';
                                }

                                cat_block.find('.near-slider').append($('<div>' +
                                '<div class="near-slider-item clearfix" data-id="' + coupon._id + '">' +
                                '<img alt="" src="' + coupon.couponImage + '">' +
                                '<h3>' + coupon.couponName + '</h3>' +
                                '<b>' + coupon.MarketingStatement + '</b>' +
                                '<span class="near-item-desc"><span data-t="valid_until">בתוקף עד</span>: ' + moment(coupon.dateTo).format('D.M.YYYY') + '</span>' +
                                '<div class="near-slider-bottom">' +
                                '<span class="near-price">' +
                                '<span class="near-price-old price_elm"><span>₪</span><span>' + coupon.tariff + '</span></span>' +
                                '<span class="near-price-new price_elm"><span>₪</span><span>' + coupon.cellPrice + '</span></span>' +
                                '</span>' +
                                '</div>' +
                                '<a class="near-item-more show_coupon_details" href="#" data-t="coupon_more">לפרטים</a>' +
                                saveButton +
                                '</div>' +
                                '</div>'));
                          }
                        }

                        //hide empty categories
                        if ($(cat_block.get(0).children[1]).find('img').length == 0) {
                            continue;
                        }

                        $('#general_view .all_coupons').append(cat_block);
                    }
                }

                Language.translate();

                $('#general_view .show_coupon_details').on('click', function(e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();

                    var id = $(this).parents('.near-slider-item').data('id');
                    Navigation.changePage('user_coupon', {id: id});
                });

                $('#general_view .near-item-save').on('click', function(e) {
                    e.stopImmediatePropagation();

                    var button = $(this);
                    var id = $(button).parents('.near-slider-item').data('id');

                    if (button.hasClass('archived')) {
                        return false;
                    }

                    if (button.hasClass('saved')) {

                        //instead of adding coupon to archive, returning it to normal state
                        API._post('/coupons/currUser/favourite', function (data) {

                            button.removeClass('saved');
                            button.find('span').data('t', 'give_it_to_me');

                            Navigation.alert_ok(Language.scheme.alerts.thank_you_for_using_benefit, function () {
                                Navigation.changePage('user_coupons');
                            });

                        }, { couponId: id }, true);
                    } else {
                        API._post('/coupons/currUser/favourite', function(data) {

                            if (data.isCouponFavourite == true) {
                                button.addClass('saved');
                                button.find('span').data('t', 'use_it');
                            } else {
                                button.removeClass('saved');
                                button.find('span').data('t', 'give_it_to_me');
                            }

                            var couponDateTo = moment(button.attr('date-to').substr(0, 10).split('-'));
                            var today = new Date();
                            var currentDate = moment([today.getFullYear(), today.getMonth()+1, today.getDate()]);
                            var daysToExpire = couponDateTo.diff(currentDate, 'days') + 1;

                            Navigation.alert(Language.scheme.messages.use_it_before_alert + daysToExpire + Language.scheme.messages.use_it_after_alert, function(){
                                Navigation.changePage('user_coupons');
                            });

                        }, {couponId: id}, true);
                    }

                    Language.translate();
                });

                setTimeout(function(){
                    $('#general_view .near-top-edit input[type="checkbox"]').on('change', function(e){
                        var id = $(this).data('cat_id');
                        var object = $('#general_view .near-item-row[data-cat_id="' + id + '"]');

                        if ($(this).is(':checked')) {
                            object.show();
                        } else {
                            object.hide();
                        }

                        var hiddenRows = 0;
                        $('#general_view .near-item-row').each(function(){
                            if($(this).css('display') == 'none'){
                                hiddenRows++;
                            }
                        });

                        $('#noBenefits').remove();
                        if($('#general_view .near-item-row').length == hiddenRows){
                            $('#general_view .near-item-row').parent().append('<div id="noBenefits">' + Language.scheme.messages.no_coupons + '</div>');
                        }
                    });
                }, 1000);

                var direction = $('body').attr('direction');
                var rtl = false;
                if (direction == 'rtl') {
                    rtl = true;
                }

                $("#general_view .near-slider").owlCarousel({lazyLoad:true,stagePadding:20,items:3,rtl:rtl,responsive:{0:{items:1},360:{items:2},600:{items:3}}});

                bindMfpPopup();
                Navigation.hideLoader();
            }
        , {categories: true}, true);
    }, 500);
}

Navigation.controllers['category'] = function(data) {
    var id = data.id;

    var category_name = '';
    var category_icon = '';
    // Populate categories dropdown list
    API._get('/coupons/categories?lang=' + Language.language, function(data) {
        // $('#general_view .filter-icons a').html('');
        for (var i in data.categories) {
            var c = data.categories[i];

            if (c._id == id) {
                category_name = c.name;
                category_icon = c.icon;
                $('#general_view .filter span').text(category_name);
            }

            $('#general_view .filter-icons').append('<a href="javascript:void(0);" data-id="' + c._id + '"><i>' + c.icon + '</i><span>' + c.name + '</span></a>')
        }

        $('#general_view .filter-icons a').on('click', function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            var id = $(this).data('id');
            Navigation.changePage('category', {id: id});
        });
    }, {}, false);

    var userCouponsIds = [];
    API._get('/coupons/currUser', function (data2) {
        if (data2.couponsByUser.length > 0) {
            for (var i in data2.couponsByUser) {
                userCouponsIds.push(data2.couponsByUser[i]._id);
            }
        }

    }, {}, false);

    API._get('/coupons', function (data) {
        var filteredCategory = data.coupons._id;

        var dateObject = new Date();

        var minutes = dateObject.getMinutes();

        if (minutes < 10) {
            minutes = '0' + minutes;
        }

        var current_time = dateObject.getHours() + ':' + minutes;
        $('.near-top-update .refreshed_time').text(current_time);

        var catsFilter = $('#general_view .near-top-edit form');
        catsFilter.find('fieldset').remove();

        var cat_block = $(
            '<div class="near-item-row" data-cat_id="' + id + '">' +
            '<h3 class="category-header"><i>'+category_icon+'</i><span>' + category_name + '</span></h3>' +
            '<div class="near-slider clearfix"></div></div>');

        // catsFilter.append(
        //     '<fieldset>' +
        //     '<input checked="checked" id="cat_' + data.couponsNear[cat]._id + '" data-cat_id="' + data.couponsNear[cat]._id + '" type="checkbox">' +
        //     '<label for="cat_' + data.couponsNear[cat]._id + '">' + data.couponsNear[cat].name + '</label>' +
        //     '</fieldset>'
        // );
        var good_coupons = 0;
        for (var c in data.coupons.coupons) {

            var coupon = data.coupons.coupons[c];
            var prev_page = sessionStorage.getItem("user_only");
            if (prev_page) {
                if (userCouponsIds.indexOf(coupon._id) != -1) { }
                else
                    continue;
            }
            if (moment(coupon.dateTo).unix() < moment().startOf('day').unix()) {
                continue;
            }
            if (coupon.status == 'onAir') {
                good_coupons++;
                var saveButton = '<a class="near-item-save"><i>v</i><span data-t="give_it_to_me">תן לי את זה</span></a>';
                // If this coupon is already bookmarked - change "save" button style
                if (userCouponsIds.indexOf(coupon._id) != -1) {
                    saveButton = '<a class="near-item-save saved"><i>v</i><span data-t="use_it">תן לי את זה</span></a>';
                }

                //TODO: bug
                //if (archivedCouponsIds.indexOf(coupon._id) != -1) {
                  //  saveButton = '<a class="near-item-save archived"><i>v</i><span data-t="archived"></span></a>';
                //}

                cat_block.find('.near-slider').append($('<div>' +
                    '<div class="near-slider-item clearfix" data-id="' + coupon._id + '">' +
                        '<img alt="" src="'  + coupon.couponImage + '">' +
                        '<h3>' + coupon.couponName + '</h3>' +
                        '<b>' + coupon.MarketingStatement + '</b>' +
                        '<span class="near-item-desc"><span data-t="valid_until">בתוקף עד</span>: ' + moment(coupon.dateTo).format('D.M.YYYY, H:mm a') + '</span>' +
                        '<div class="near-slider-bottom">' +
                            '<span class="near-price">' +
                                '<span class="near-price-old price_elm"><span>₪</span><span>' + coupon.tariff + '</span></span>' +
                                '<span class="near-price-new price_elm"><span>₪</span><span>' + coupon.cellPrice + '</span></span>' +
                            '</span>' +
                        '</div>' +
                        '<a class="near-item-more show_coupon_details" href="#" data-t="coupon_more">לפרטים</a>' +
                        saveButton +
                    '</div>' +
                '</div>'));
            }
        }


        $('#general_view .near-items').append(cat_block);

        Language.translate();

        $('#general_view .show_coupon_details').on('click', function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            var id = $(this).parents('.near-slider-item').data('id');
            Navigation.changePage('user_coupon', {id: id});
        });

        $('#general_view .near-item-save').on('click', function() {
            var button = $(this);
            var id = $(button).parents('.near-slider-item').data('id');

            if (button.hasClass('archived')) {
                return false;
            }

            if (button.hasClass('saved')) {
                API._post('/coupons/currUser/archive', function(data) {
                    button.removeClass('saved').addClass('archived');
                    button.find('span').data('t', 'archived');

                    Navigation.alert_ok(Language.scheme.alerts.thank_you_for_using_benefit, function() {
                        Navigation.changePage('user_coupons');
                    });
                }, {couponId: id}, false);
            } else {
                API._post('/coupons/currUser/favourite', function(data) {
                    console.log(data);

                    if (data.isCouponFavourite == true) {
                        button.addClass('saved');
                        button.find('span').data('t', 'use_it');
                    } else {
                        button.removeClass('saved');
                        button.find('span').data('t', 'give_it_to_me');
                    }
                }, {couponId: id}, false);
            }

            Language.translate();
        });

        $('#general_view .near-top-edit input[type="checkbox"]').on('change', function(e){
            var id = $(this).data('cat_id');
            var object = $('#general_view .near-item-row[data-cat_id="' + id + '"]');

            if ($(this).is(':checked')) {
                object.show();
            } else {
                object.hide();
            }
        });


        $('#general_view .filter-icons a').on('click', function(e) {
            var id = $(this).data('id');

            Navigation.changePage('category', {id: id});
        });


        // $('.loaderDiv').fadeOut(300);

        var direction = $('body').attr('direction');
        var rtl = false;
        if (direction == 'rtl') {
            rtl = true;
        }

        $('#noBenefits').remove();
        if (good_coupons == 0) {
            $('#general_view .near-item-row').parent().append('<div id="noBenefits">' + Language.scheme.messages.no_coupons + '</div>');
            $('#general_view .near-item-row').hide();
        }

        $("#general_view .near-slider").owlCarousel({lazyLoad:true,stagePadding:20,items:3,rtl:rtl,responsive:{0:{items:1},360:{items:2},600:{items:3}}});
    }, {category: id});
}