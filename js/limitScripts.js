Navigation.controllers['limit'] = function() {
    if (
        (typeof localStorage['createCoupon'] == 'undefined') &&
        (typeof localStorage['editCoupon'] == 'undefined')
    ) {
        Navigation.changePage('main');
        return false;
    }

    var edit = false;

    // Get old data from previous forms and\or edited coupon
    if (typeof localStorage['createCoupon'] == 'undefined') {
        var oldData = JSON.parse(localStorage.editCoupon);
        edit = true;
    } else {
        var oldData = JSON.parse(localStorage.createCoupon);
    }

    oldData['userId'] = JSON.parse(localStorage.user)._id;

    // Set default radius if not specified
    var radiusStart = 10;
    if (typeof oldData['radius'] != 'undefined') {
        radiusStart = oldData['radius'];
    }

    // Init range slider
    $('#general_view #range-slider').noUiSlider({
        start: radiusStart,
        step:5,
        connect: "lower",
        direction: "rtl",
        range: {
            'min': 1,
            'max': 25
        },
        serialization: {
            lower: [
                $.Link({
                    target: $("#field")
                })
            ]
        }
    });

    // Used for editing
    for (var i in oldData) {
        if ($("#general_view [name='" + i + "']").attr('type') == 'checkbox') {
            if (oldData[i]) {
                $("#general_view [name='" + i + "']").attr('checked', 'checked');
            } else {
                $("#general_view [name='" + i + "']").removeAttr('checked');
            }
        } else {
            $("#general_view [name='" + i + "']").val(oldData[i]);
        }
    }


    var regularPrice = oldData['tariff'];
    var specialPrice = oldData['cellPrice'];

    // Value price in raw currency
    var valuePriceRaw = regularPrice - specialPrice;
    var valuePrice = 0;
    // As we calculate everything in USD, check what is current currency and convert
    // to USD if needed.
    //if (Language.scheme.config.currency_code != 'USD') {
    //    valuePrice = API._convertCurrency(Language.scheme.config.currency_code + '_USD', valuePriceRaw);
    //} else {
        valuePrice = valuePriceRaw;
    //}

    var clickPrice = 0;

    var baseClick = 0;
    var additionLimit = 0;
    var additionStep = 0;
    var additionClickStep = 0;
    var maxClick = 0;

    if (valuePrice <= 6.4) {
        baseClick = 0.06;
        additionLimit = 2.6;
        additionStep = 1.3;
        additionStepClick = 0.026;
        maxClick = 0.14;
    } else if (valuePrice > 6.4 && valuePrice <= 12.8) {
        baseClick = 0.9;
        additionLimit = 10.3;
        additionStep = 1.3;
        additionStepClick = 0.25;
        maxClick = 0.14;
    } else if (valuePrice > 12.8 && valuePrice <= 25.6) {
        baseClick = 0.102;
        additionLimit = 15.4;
        additionStep = 2.6;
        additionStepClick = 0.13;
        maxClick = 0.18;
    } else if (valuePrice > 25.6 && valuePrice <= 51.2) {
        baseClick = 0.115;
        additionLimit = 30.8;
        additionStep = 5.1;
        additionStepClick = 0.13;
        maxClick = 0.192;
    } else if (valuePrice > 51.2 && valuePrice <= 102.6) {
        baseClick = 0.128;
        additionLimit = 64.1;
        additionStep = 6.4;
        additionStepClick = 0.13;
        maxClick = 0.205;
    } else if (valuePrice > 102.6 && valuePrice <= 256.4) {
        baseClick = 0.141;
        additionLimit = 128;
        additionStep = 5.1;
        additionStepClick = 0.13;
        maxClick = 0.45;
    } else if (valuePrice > 256.4) {
        baseClick = 0.23;
        additionLimit = 256.4;
        additionStep = 26.6;
        additionStepClick = 0.2;
        maxClick = 1;
    }

    clickPrice = baseClick;

    if (valuePrice >= additionLimit) {
        clickPrice += parseInt((valuePrice - additionLimit) / additionStep) * additionStepClick;
    }

    if (clickPrice > maxClick) {
        clickPrice = maxClick;
    }

    //if (Language.scheme.config.currency_code != 'USD') {
    //    clickPrice = API._convertCurrency('USD_' + Language.scheme.config.currency_code, clickPrice);
    //}

    // Keep only tow digits after comma
    oldData['clickPrice'] = clickPrice.toFixed(2);
    $('#general_view [name=clickPrice]').val(oldData['clickPrice']);

    // Fill clicks limit dropdown
    var clickLimits = ['-', 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000, 1500, 2000, 3000, 4000, 5000];
    for (var i = 0; i < clickLimits.length; i++) {
        var clicksValue = clickLimits[i];
        $('#general_view select[name="maxClicks"]').append('<option value="' + clicksValue + '">' + clicksValue + '</option>');
    }

    // Trigger total price to change when max clicks value is changed
    $('#general_view select[name="maxClicks"]').on('change', function() {
        var value = $(this).val();

        if (value != '-') {
            var result = (parseFloat($('#general_view input[name="clickPrice"]').val()) * parseInt(value)).toFixed(2);
            $('#general_view input[name="maxBudget"]').val(result);
            $('#general_view .budgetValue').text(result + ' ' + Language.scheme.config.currency_code);
        } else {
            $('#general_view input[name="maxBudget"]').val('');
            $('#general_view .budgetValue').text('-');
        }
    });

    if (edit) {
        API._get('/coupons/' + localStorage.editId, function(e) {
            var originalCoupon = e.coupon;

            // if (originalCoupon.clickPrice == clickPrice) {
            $('#general_view [name=maxClicks]').val(originalCoupon.maxClicks);
            $('#general_view [name=maxClicks]').trigger('change');

            if (originalCoupon.generalBonus == false) {
                $('#general_view [name=generalBonus]').removeAttr('checked');
            }

            if (originalCoupon.receiveBonusUpdates == false) {
                $('#general_view [name=receiveBonusUpdates]').removeAttr('checked');
            }
            // }
        }, {}, false);
    }


    $('#general_view input[name="maxBudget"]').on('keydown', function() {
        $('#general_view select[name="maxClicks"]').val('-');

        $('#general_view .budgetValue').text($(this).val() + ' ' + Language.scheme.config.currency_code);
    });

    $('select[name=maxClicks]').on('change', function(e){
        $('#maxBudget').parent().removeClass().addClass( ($(this).val().length === 1) ? 'invalid' : 'valid' );
    });

    if (edit) {
        API._get('/coupons/' + localStorage.editId, function(e) {
            var originalCoupon = e.coupon;

            // if (originalCoupon.clickPrice == clickPrice) {
            $('#general_view [name=maxClicks]').val(originalCoupon.maxClicks);
            $('#general_view [name=maxClicks]').trigger('change');

            if (originalCoupon.generalBonus == false) {
                $('#general_view [name=generalBonus]').removeAttr('checked');
            }

            if (originalCoupon.receiveBonusUpdates == false) {
                $('#general_view [name=receiveBonusUpdates]').removeAttr('checked');
            }
            // }
        }, {}, false);
    }


    $('#general_view input[name="maxBudget"]').on('keydown', function() {
        $('#general_view select[name="maxClicks"]').val('-');

        $('#general_view .budgetValue').text($(this).val() + ' ' + Language.scheme.config.currency_code);
    });

    //$('select[name=maxClicks]').on('change', function(e){
    //    $('#maxBudget').parent().removeClass().addClass( ($(this).val().length === 1) ? 'invalid' : 'valid' );
    //});
    //
    //$('#maxBudget').on('change', function(e) {
    //    $(this).parent().removeClass().addClass( ( $(this).val() === '' ||  $(this).val().length === 0 )? 'invalid' : 'valid' );
    //});

    // Going to next step
    $('#general_view #coupon_limit_form input[type="submit"]').on('click', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();


        if( $('#general_view input[name=generalBonus]').is(':checked') ) {
            localStorage.setItem('generalBenefit', true);
        }

        Navigation.showLoader();

        setTimeout(function(){
            if (!$('#general_view input[name=i_agree_monthly_pay]').is(':checked')) {
                Navigation.alert(Language.scheme.alerts.you_must_agree);
                return false;
            }

            // Get data from current form
            var formDataRaw = $('#general_view #coupon_limit_form').serializeArray();
            var formData = [];
            for (var i in formDataRaw) {
                oldData[formDataRaw[i].name] = formDataRaw[i].value;
            }

            if (typeof oldData['couponNumber'] == 'undefined' || oldData['couponNumber'] == '') {
                oldData['couponNumber'] = 1;
            }

            oldData['radius'] = $('#general_view .radius_value').text();
            oldData['publishOnWall'] = true;

            if (typeof oldData['generalBonus'] == 'undefined') {
                oldData['generalBonus'] = false;
            }

            //TODO: Какого-то хрена без этого не создается бенефит, если не указать локейшн
            if(typeof oldData['lat'] == 'undefined') {
                oldData['lat'] = '0';
                oldData['lng'] = '0';
            }

            var valid = true;
            API._post('/coupons/validation', function(e, statusCode) {
                $('#general_view input[type!="submit"]').removeClass('oops');

                if (statusCode == 400) {
                    var field_names = [];

                    $('#general_view input[type!="submit"]').each(function(){
                        field_names.push($(this).attr('name'));
                    });

                    for (var i in e) {
                        var elm = $('#general_view input[name="' + e[i].param + '"]');

                        if (elm.length != 0 && ignoredFields.indexOf(e[i].param) == -1) {
                            elm.parent('fieldset').addClass('invalid');
                            elm.focus();
                            console.log(e[i]);
                            valid = false;
                        }
                    }
                }
            }, oldData, false);

            if (!valid) {
                return false;
            }

            var couponId = localStorage.editId;

            // Convert to FormData for sending
            var cData = new FormData();
            var ignoredFields = ['couponImage'];
            for (var i in oldData) {
                if (ignoredFields.indexOf(i) === -1) {
                    cData.append(i, oldData[i]);
                }
            }

            // Convert image for sending
            if (typeof oldData['couponImage'] != 'undefined' && oldData['couponImage'] != '') {
                var blobBin = atob(oldData['couponImage']/*.split(',')[1]*/);
                var array = [];
                for (var i = 0; i < blobBin.length; i++) {
                    array.push(blobBin.charCodeAt(i));
                }

                var png = undefined;
                try {
                    png = new Blob([new Uint8Array(array)], { type: 'image/png' });
                } catch(e) {
                    // $('#catched_errors').append('-|-' + JSON.stringify(e) + '; ' + e.name + '-|-');

                    // TypeError old chrome and FF
                    window.BlobBuilder = window.BlobBuilder ||
                    window.WebKitBlobBuilder ||
                    window.MozBlobBuilder ||
                    window.MSBlobBuilder;
                    if(e.name == 'TypeError' && window.BlobBuilder){
                        var bb = new BlobBuilder();
                        bb.append([new Uint8Array(array)]);
                        png = bb.getBlob('image/png');
                    }
                    else if(e.name == "InvalidStateError"){
                        // InvalidStateError (tested on FF13 WinXP)
                        png = new Blob([new Uint8Array(array)], {type : 'image/png'});
                    }
                    else{
                        // We're screwed, blob constructor unsupported entirely
                        Navigation.alert('We\'re screwed, blob constructor unsupported entirely');
                    }

                    // $('#catched_errors').append(png);
                } finally {
                    if (typeof png != 'undefined') {
                        cData.append('couponImage', png);
                    }
                }
            }

            // Convert logo image  for sending
            if (typeof oldData['logo'] != 'undefined' && oldData['logo'] != '') {
                var blobBinLogo = atob(oldData['logo']);
                var arrayLogo = [];
                for (var k = 0; k < blobBinLogo.length; k++) {
                    arrayLogo.push(blobBinLogo.charCodeAt(k));
                }

                var pngLogo = undefined;
                try {
                    pngLogo = new Blob([new Uint8Array(arrayLogo)], { type: 'image/png' });
                } catch(e) {

                    // TypeError old chrome and FF
                    window.BlobBuilder = window.BlobBuilder ||
                    window.WebKitBlobBuilder ||
                    window.MozBlobBuilder ||
                    window.MSBlobBuilder;
                    if(e.name == 'TypeError' && window.BlobBuilder){
                        var bbLogo = new BlobBuilder();
                        bbLogo.append([new Uint8Array(arrayLogo)]);
                        pngLogo = bbLogo.getBlob('image/png');
                    }
                    else if(e.name == "InvalidStateError"){
                        // InvalidStateError (tested on FF13 WinXP)
                        pngLogo = new Blob([new Uint8Array(arrayLogo)], {type : 'image/png'});
                    }
                    else{
                        // We're screwed, blob constructor unsupported entirely
                        Navigation.alert('We\'re screwed, blob constructor unsupported entirely');
                    }
                } finally {
                    if (typeof pngLogo != 'undefined') {
                        cData.append('logo', pngLogo);
                    }
                }
            }

            if (edit) {
                API._put('/coupons/' + couponId, function(data, statusCode) {
                    if (statusCode !== '400') {
                    } else {
                        valid = false;
                        Navigation.alert('check_fields_or_try_later');
                    }
                }, cData, false);
            } else {
                API._post('/coupons', function(data, statusCode, responseText) {
                    if (statusCode !== '400') {
                        couponId = data.coupon._id;
                        API._patch('/coupons/' + couponId + '/status', function(){
                        }, {status: 'inProcess'}, false);
                    } else {
                        valid = false;
                        Navigation.alert('check_fields_or_try_later');
                    }
                }, cData, false);
            }

            if (!valid) {
                return false;
            }

            oldData['couponId'] = couponId;
            Navigation.changePage('charge', {objectData: oldData, edit: edit});
            return false;
        }, 500);
    });
};