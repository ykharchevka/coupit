Navigation.controllers['create_coupon'] = function() {
    if (typeof localStorage.createCoupon !== 'undefined') {
        manageCoupon(JSON.parse(localStorage.createCoupon));
    } else {
        manageCoupon();
    }
    $('#step1 [name="linkToFacebook"]:first').focus( function(e) {
        $('#step1 #spacer').show(0);
        var offset = $('#step1 [name="linkToFacebook"]:first').offset(); // Contains .top and .left
        offset.top -= 350;
        $("html, body").animate({
            scrollTop: offset.top,
        }, "slow");
        //$("html, body").animate({ scrollTop: $(document).height() }, "slow");
    });
    $('#step1 [name="linkToFacebook"]:first').focusout( function(e) {
        $('#step1 #spacer').hide(0);
    });

    function validateInput($input) {
        var fieldset = $input.parents('fieldset');
        var name = $input.attr('name');
        var value = $input.val();

        var dataArray = $input.closest('form').serializeArray();
        var formData = serializedToObject(dataArray);

        formData.dateTo = moment(formData.dateTo).unix();
        formData.dateFrom = moment(formData.dateFrom).unix();

        // For additional info visit http://livr-spec.org/

        var couponRules = {
            couponName:         'not_empty',
            MarketingStatement: 'not_empty',
            couponDescription:  'not_empty',
            dateTo:             ['required', 'positive_integer', { 'min_number': formData.dateFrom }],
            dateFrom:           ['required', 'positive_integer', { 'max_number': formData.dateTo }],
            businessName:       'not_empty',
            contactName:        'not_empty',
            tariff:             ['required', 'positive_integer', { 'min_number': formData.cellPrice }],
            cellPrice:          ['required', 'positive_integer', { 'max_number': formData.tariff }],
            contactPhone:       ['required', 'positive_integer'],
            storePhone:         ['required', 'positive_integer'],
            storeEmail:         ['required', 'email'],
            logo:               'not_empty',
        };

        var validator = new LIVR.Validator(couponRules);

        var isValidated = validator.validate(formData);
        var errors = validator.getErrors();
        console.log('errors',errors);

        var isValid = !errors[name];

        fieldset.attr('class', isValid ? 'valid' : 'invalid');
    }

    var connectedFields = {
        tariff: 'cellPrice',
        cellPrice: 'tariff',
        dateTo: 'dateFrom',
        dateFrom: 'dateTo'
    };

    $('#createCouponForm').find('input[class=required], textarea[class=required]').on('change', function(e) {
        var $input = $(e.target);
        var name = $input.attr('name')

        validateInput($input);

        var connectedField = connectedFields[name];

        if (connectedField) {
            var $connectedInput = $input.closest('form').find('input[name="'+connectedField+'"]');

            if ($connectedInput.val()) {
                validateInput($connectedInput);
            }
        }
    });
};

Navigation.controllers['edit_coupon'] = function(data) {
    // $('body').append('<iframe src="http://maps.googleapis.com/maps/api/staticmap?center=New+York,NY&zoom=13&size=600x300&key=AIzaSyAaFGXmR_ZrpDlRDBE42KcAdb9gwqRImfY"></iframe>');
    $('#step1 [name="linkToFacebook"]:first').focus( function(e) {
        $('#step1 #spacer').show(0);
        var offset = $('#step1 [name="linkToFacebook"]:first').offset(); // Contains .top and .left
        offset.top -= 350;
        $("html, body").animate({
            scrollTop: offset.top
        }, "slow");
        // $("html, body").animate({ scrollTop: $(document).height() }, "slow");
    });
    $('#step1 [name="linkToFacebook"]:first').focusout( function(e) {
        $('#step1 #spacer').hide(0);
    });
    var id = data.id;
    var coupon;

    API._get('/coupons/' + id, function(data) {
        coupon = data['coupon'];
    }, {}, false);
	if (typeof id != 'undefined') {
		$('#general_view #endStep1').attr('data-t-v','save');
	}
    manageCoupon(coupon, id);
};


var manageCoupon = function(couponData, editId) {
    function initialize() {

    }

    google.maps.event.addDomListener(window, 'load', initialize);

    function sendImage(dataURL) {
        var dataURL = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAK1JREFUeNqUkjEKxiAMhZtoezkXB0/g0isUuvcm3cRRipuXsuP/pxRKCEVqwOGTPN8LEYwxQ09pROwTUHUJBmvtj9W+7w10zqFSiusBoIEUB0UkMZLAcRw1nZRSrfW+IsMQwtMh8Hrde89TxhgbOM8zkgM3FSMJpGacpqlLoElQSjnP8+k4joMLOF5xlmXhKXPODVzXtW+Glz20BYSwbRt5ffxHtHj43n3XX4ABALq6m4GkkftzAAAAAElFTkSuQmCC"
        var blobBin = atob(dataURL.split(',')[1]);
        var array = [];
        for (var i = 0; i < blobBin.length; i++) {
            array.push(blobBin.charCodeAt(i));
        }
        //alert(dataURL);
        return new Blob([new Uint8Array(array)], { type: 'image/png' });
    }

    $('#general_view').find('input[name="couponName"], input[name="cellPrice"], input[name="tariff"], input[name="dateFrom"], input[name="dateTo"]').on('change', function() {
        if (!$(this).val()) {
            $(this).css({'border-color': 'red', 'border-width': '2px'});
        } else {
            $(this).css({'border-color': '', 'border-width': ''});
        }
    });

    API._get('/coupons/categories?lang=' + Language.language, function(data) {
        for (var i in data.categories) {
            var c = data.categories[i];

            $('<input name="category" type="checkbox" id="id_' + c._id + '" value="' + c._id + '"><label for="id_' + c._id + '">' + c.name + '</label>').appendTo('#general_view #categories_list .half:eq(' + (i % 2) + ')');
        }
    }, {}, false);

    var userData = JSON.parse(localStorage.user);
    var createCouponForm = $('#createCouponForm');


    //$.each(userData, function(key, value){
    //    if(createCouponForm.find('input[name=' + key + ']')){
    //        $(this).val(value);
    //    }
    //
    //});
    //Циклом не хочет, т.к. по разному поля называются, будем костылем
    createCouponForm.find('input[name=storeEmail]').val(userData.email);
    createCouponForm.find('input[name=businessName]').val(userData.businessName);
    createCouponForm.find('input[name=contactName]').val(userData.username);
    createCouponForm.find('input[name=contactPhone]').val(userData.cell);
    createCouponForm.find('input[name=storePhone]').val(userData.businessPhone);

    var map,
        marker,
        mapOptions,
        myLatlng;

    $('#general_view .show_map').on('click', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        var button = $(this);
        button.append('<span class="clock" style="font-family: coupit">g</span>');

        navigator.geolocation.getCurrentPosition(
            function(position) {
                var lat, lng;
                button.find('.clock').remove();

                if (couponData && couponData.hasOwnProperty('loc')) {
                    lat = couponData.loc[0];
                    lng = couponData.loc[1];
                } else {
                    lat = position.coords.latitude;
                    lng = position.coords.longitude;
                }

                $('#location_map').data('lat', lat).data('lng', lng);
                $('#general_view input[name=lat]').val(lat);
                $('#general_view input[name=lng]').val(lng);

                myLatlng = new google.maps.LatLng(lat,lng);

                mapOptions = {
                    zoom: 16,
                    center: myLatlng
                };

                $('#location_map')
                    .html('')
                    .css({
                        display: 'block',
                        opacity: 1,
                        height: $(window).height() / 3
                    });

                map = new google.maps.Map( document.getElementById('location_map'), mapOptions );

                marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map
                });

                marker.setMap(map);

                $('#manualCoords').fadeIn(1000).appendTo($('#location_map').parent());
                $('#use_this_pos').addClass('location');

                google.maps.event.addListener(map, 'click', function(e) {
                    marker.setMap(null);

                    $('#general_view #location_map').data('lat', e.latLng.D).data('lng', e.latLng.k);
                    $('#general_view input[name="lat"]').val(e.latLng.D);
                    $('#general_view input[name="lng"]').val(e.latLng.k);

                    marker = new google.maps.Marker({
                        position: e.latLng,
                        map: map
                    });
                });
            },
            function (error) {
               Navigation.alert(Language.scheme.alerts.please_turn_gps_on, function() {
                   button.find('.clock').remove();
               });
            },
            {
                timeout: 7000
            }
        );
    });

    $('#general_view #use_this_pos').on('click', function() {
        Navigation.alert_set_coords_manually('', function(){
            navigator.geolocation.getCurrentPosition(
                function(position) {
                    var lat, lng;

                    if (couponData && couponData.hasOwnProperty('loc')) {
                        lat = couponData.loc[0];
                        lng = couponData.loc[1];
                    } else {
                        lat = position.coords.latitude;
                        lng = position.coords.longitude;
                    }

                    $('#location_map').data('lat', lat).data('lng', lng);
                    $('#general_view input[name=lat]').val(lat);
                    $('#general_view input[name=lng]').val(lng);

                    myLatlng = new google.maps.LatLng(lat,lng);

                    mapOptions = {
                        zoom: 16,
                        center: myLatlng
                    };

                    $('#location_map')
                        .html('')
                        .css({
                            display: 'block',
                            opacity: 1,
                            height: $(window).height() / 3
                        });

                    var button = $('#general_view .show_map');
                    button.append('<span class="clock" style="font-family: coupit">g</span>');


                    var map = new google.maps.Map( document.getElementById('location_map'), mapOptions );

                    google.maps.event.addListener(map, 'click', function(e) {
                        marker.setMap(null);

                        $('#general_view #location_map').data('lat', e.latLng.D).data('lng', e.latLng.k);
                        $('#general_view input[name="lat"]').val(e.latLng.D);
                        $('#general_view input[name="lng"]').val(e.latLng.k);

                        marker = new google.maps.Marker({
                            position: e.latLng,
                            map: map
                        });
                    });

                    setTimeout(function() {
                        var geocoder = new google.maps.Geocoder();

                        //TODO: maybe needed country later
                        //var country = document.getElementById("country").value;
                        var city = document.getElementById("locality").value;
                        var street = document.getElementById("route").value;
                        var buildingNumber = document.getElementById("buildingNumber").value;

                        //TODO: maybe needed country later
                        //var address = country +", " + city + ", " + street +", " + buildingNumber;
                        var address = city + ", " + street +", " + buildingNumber;

                        geocoder.geocode( { 'address': address}, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                map.setCenter(results[0].geometry.location);
                                marker = new google.maps.Marker({
                                    map: map,
                                    position: results[0].geometry.location
                                });
                                button.find('.clock').remove();
                            } else {
                                alert('Geocode was not successful for the following reason: ' + status);
                            }
                        });
                    }, 500);

                    $('#manualCoords').fadeIn(1000).appendTo($('#location_map').parent());
                    $('#use_this_pos').addClass('location');
                },
                function (error) {
                },
                {
                    timeout: 7000
                }
            );

        });
    });

    $('#general_view #cancel_this_pos').on('click', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        var button = $(this);
        button.append('<span class="clock" style="font-family: coupit">g</span>');

        navigator.geolocation.getCurrentPosition(
            function(position) {
                var lat, lng;
                button.find('.clock').remove();

                if (couponData && couponData.hasOwnProperty('loc')) {
                    lat = couponData.loc[0];
                    lng = couponData.loc[1];
                } else {
                    lat = position.coords.latitude;
                    lng = position.coords.longitude;
                }

                $('#location_map').data('lat', lat).data('lng', lng);
                $('#general_view input[name=lat]').val(lat);
                $('#general_view input[name=lng]').val(lng);

                myLatlng = new google.maps.LatLng(lat,lng);

                mapOptions = {
                    zoom: 16,
                    center: myLatlng
                };

                map = new google.maps.Map( document.getElementById('location_map'), mapOptions );

                marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map
                });

                marker.setMap(map);
            },
            function (error) {
                Navigation.alert(Language.scheme.alerts.please_turn_gps_on, function() {
                    button.find('.clock').remove();
                });
            },
            {
                timeout: 7000
            }
        );
    });

    if (typeof couponData !== 'undefined') {
        var createCouponData = couponData;

        if (typeof createCouponData['workTime'] == 'undefined') {
            createCouponData['workTime'] = {
                workdays: createCouponData['workdays'],
                workdaysHoursFrom: createCouponData['workdaysHoursFrom'],
                workdaysHoursTo: createCouponData['workdaysHoursTo'],
                sunday: createCouponData['sunday'],
                sundayHoursFrom: createCouponData['sundayHoursFrom'],
                sundayHoursTo: createCouponData['sundayHoursTo'],
                saturday: createCouponData['saturday'],
                saturdayHoursFrom: createCouponData['saturdayHoursFrom'],
                saturdayHoursTo: createCouponData['saturdayHoursTo']
            };
        }

        if (typeof createCouponData['categories'] == 'undefined') {
            createCouponData['categories'] = createCouponData['categoryIds'].split(',');
        }

        for (var i in createCouponData) {
            if (i == 'categories') {
                var categories = createCouponData[i]/*.split(',')*/;

                $('#general_view [name="category"]').each(function() {
                    if (categories.indexOf($(this).val()) !== -1) {
                        $(this).attr('checked', 'true');
                    }
                });
            } else if (i == 'couponImage' && createCouponData[i] != '') {
                console.log("i == 'couponImage'");
                var imageObject = $("#general_view #createCouponForm #imageField #smallImage");

                var imageUrl = '';

                if (createCouponData[i].indexOf('http://') != -1) {
                    imageUrl = createCouponData[i];
                } else {
                    imageUrl = 'data:image/png;base64,' + createCouponData[i];
                }

                // $('#general_view input[name=couponImage]').val(imageUrl);
                imageObject.attr('src', imageUrl);
                imageObject.css('display', 'block');
            } else if (i == 'dateFrom' || i == 'dateTo') {
                $('#general_view input[name="' + i + '"]').val(moment(createCouponData[i]).format('YYYY-MM-DD'))
            } else if (i == 'logo' && createCouponData[i] != '') {
                var imageObjectLogo = $("#general_view #createCouponForm #imageFieldLogo #smallImageLogo");
                var imageUrlLogo = '';

                if (createCouponData[i].indexOf('http://') != -1) {
                    imageUrlLogo = createCouponData[i];
                } else {
                    imageUrlLogo = 'data:image/png;base64,' + createCouponData[i];
                }

                imageObjectLogo.attr('src', imageUrlLogo);
                imageObjectLogo.css('display', 'block');
            } else if (i == 'loc') {
                $('#general_view input[name="lat"]').val(createCouponData[i][0]);
                $('#general_view input[name="lng"]').val(createCouponData[i][1]);
            } else if (i == 'workTime') {

                for (var j in createCouponData[i]) {
                    switch (j) {
                        case 'workdaysHoursFrom':
                        case 'workdaysHoursTo':
                        case 'sundayHoursFrom':
                        case 'sundayHoursTo':
                        case 'saturdayHoursFrom':
                        case 'saturdayHoursTo':
                            $('#general_view [name=' + j + ']').val(createCouponData[i][j]);
                        break;
                        case 'workdays':
                            if (typeof createCouponData[i][j] == 'undefined') {
                                continue;
                            }

                            var workDaysArray = createCouponData[i][j].split(',');
                            for (var k in workDaysArray) {
                                var value = workDaysArray[k].trim().toLowerCase();

                                $('#general_view [name="workdays[]"][value=' + value + ']').attr('checked', true);
                            }
                        break;
                        case 'sunday':
                            if (createCouponData[i][j]) {
                                $('#general_view [name=sunday]').attr('checked', true);
                            }
                        break;
                        case 'saturday':
                            if (createCouponData[i][j]) {
                                $('#general_view [name=saturday]').attr('checked', true);
                            }
                        break;
                    }
                }
            } else {
                if (i !== 'couponImage') {
                    $("#general_view [name='" + i + "']").val(createCouponData[i]);
                }
            }
        }

        var lat = $('#general_view input[name="lat"]').val();
        var lng = $('#general_view input[name="lng"]').val();

        if (lat != '' && lng != '') {
            $('#general_view .coords').text('Detected: ' + lat + ', ' + lng);
        }
    }

    var form = {};

    // $("#general_view #btPic").click(function () {
    //     sendImage(newBlobImg);
    // });

    // Step 1 validation
    // Moving to next step
    $('#general_view #endStep1').on('click', function () {
        if ($('#general_view [name="workdays[]"]').is(':checked')) {
            Navigation.showLoader();
        }

        setTimeout(function(){
            var HEADER_OFFSET = 265,
                OFFSET_TOP_SPEED = 400;
            // Collect and format step 1 data
            var formDataRaw = $('#general_view #createCouponForm').serializeArray();
            var formData = {};
            formData['workdays'] = [];

            for (var i in formDataRaw) {
                if (formDataRaw[i].name == 'category') {
                    if (typeof formData['category'] === 'undefined') {
                        formData[formDataRaw[i].name] = [];
                    }

                    formData[formDataRaw[i].name].push(formDataRaw[i].value);
                } else if (formDataRaw[i].name == 'workdays[]') {
                    formData['workdays'].push(formDataRaw[i].value.charAt(0).toUpperCase() + formDataRaw[i].value.slice(1));
                } else {
                    if (formDataRaw[i].value != '') {
                        formData[formDataRaw[i].name] = formDataRaw[i].value;
                    }
                }

            }
            formData['workdays'] = formData['workdays'].join(', ');

            var ignoreEmpty = ['linkToProduct', 'linkToFacebook'];
            for (var i in formData) {
                if (ignoreEmpty.indexOf(i) != -1 && formData[i] == '') {
                    delete formData[i];
                }
            }

            $('#general_view *').removeClass('invalid');


            var marketPrice = parseInt($('#createCouponForm input[name=tariff]').val(), 10);
            var offerPrice = parseInt($('#createCouponForm input[name=cellPrice]').val(), 10);

            if(offerPrice >= marketPrice) {
                $('#general_view input[name=cellPrice]').parent('fieldset').addClass('invalid');
                Navigation.alert(Language.scheme.alerts.sale_price_cannot_be_greater, function() {
                    $.scrollTo( $('#general_view input[name=cellPrice]').offset().top - HEADER_OFFSET, OFFSET_TOP_SPEED, function(){
                        $('#general_view input[name=cellPrice]').focus();
                    });
                });

                return false;
            }


            var dateToTimestamp = moment($('#createCouponForm input[name=dateTo]').val()).unix();
            var dateFromTimestamp = moment($('#createCouponForm input[name=dateFrom]').val()).unix();
            if(parseInt(dateToTimestamp) <= parseInt(dateFromTimestamp)) {
                $('#general_view input[name=dateTo]').parent('fieldset').addClass('invalid');
                Navigation.alert(Language.scheme.alerts.wrong_coupon_dates, function() {
                    $.scrollTo( $('#general_view input[name=dateTo]').offset().top - HEADER_OFFSET, OFFSET_TOP_SPEED, function(){
                        $('#general_view input[name=dateTo]').focus();
                    });
                });
                return false;
            }

            var fieldErrorPairs = {
                couponName: {minLength: 2, alert: 'coupon_must_be_filled'},
                MarketingStatement: {alert: 'marketing_statement_required'},
                couponDescription: {alert: 'coupon_description_required'},
                cellPrice: {alert: 'special_price_required'},
                tariff: {alert: 'list_price_required'},
                dateFrom: {alert: 'date_required'},
                dateTo: {alert: 'end_date_required'},
                businessName: {alert: 'business_name_required'},
                contactName: {alert: 'contact_name_required'},
                contactPhone: {minLength: 3, alert: 'contact_phone_required'},
                storePhone: {minLength: 3, alert: 'store_phone_required'},
                storeEmail: {email: true, alert: 'store_email_required'}
            };

            var validation = Language.scheme.alerts.required_before + '<br />',
                validationFlag = false;
            for (var i in fieldErrorPairs) {

                if (!formData[i] ||
                    (typeof fieldErrorPairs[i].minLength != 'undefined' &&
                    formData[i].length < fieldErrorPairs[i].minLength) ||
                    (typeof fieldErrorPairs[i].email != 'undefined' &&
                    fieldErrorPairs[i].email == true &&
                    !validateEmail(formData[i]))
                ) {
                    if(fieldErrorPairs.hasOwnProperty(i)){
                        validation += Language.scheme.alerts[fieldErrorPairs[i].alert] + '<br />';
                        $('#general_view input[name=' + i + ']').parents('fieldset').addClass('invalid');

                        if(fieldErrorPairs[i].alert == 'coupon_description_required') {
                            $('#general_view textarea[name=couponDescription]').parents('fieldset').addClass('invalid');
                        }

                        validationFlag = true;
                    }
                }
            }

            var invalidFields = $('.invalid .required');
            var invalidFieldsOffset = $(invalidFields).offset();

            if(invalidFields.length === 0) {
                validationFlag = false;
            }

            if(validationFlag){
                validation += Language.scheme.alerts.required_after;
                Navigation.alert(validation, function(){
                    setTimeout(function(){
                        $.scrollTo( invalidFieldsOffset.top - HEADER_OFFSET, OFFSET_TOP_SPEED, function(){
                            invalidFields.get(0).focus();
                            Navigation.changePage('create_coupon');
                        } );
                    }, 100);
                });
            }


            $("#general_view .required").each(function () {
                if (!$(this).val()) {
                    $(this).parents('fieldset').addClass('invalid');
                }
            });


            var cellPrice = $('#general_view [name=cellPrice]');
            var tariff = $('#general_view [name=tariff]');

            if (!validationFlag) {

                if (!$('#general_view [name="workdays[]"]').is(':checked')) {
                    Navigation.alert(Language.scheme.alerts.no_workdays_selected);
                    $('#general_view [name="workdays[]"]').addClass('invalid');
                    Navigation.hideLoader();
                }

                if (cellPrice.val() == 0) {
                    cellPrice.addClass('oops');
                }

                if (tariff.val() == 0) {
                    tariff.addClass('oops');
                }


                if (!moment('1970-01-01T' + $('#general_view input[name=workdaysHoursFrom]').val()).isValid()) {
                    $('#general_view input[name=workdaysHoursFrom]').parent('fieldset').addClass('invalid');
                }
                if (!moment('1970-01-01T' + $('#general_view input[name=workdaysHoursTo]').val()).isValid()) {
                    $('#general_view input[name=workdaysHoursTo]').parent('fieldset').addClass('invalid');
                }


                if (!moment('1970-01-01T' + $('#general_view input[name=sundayHoursFrom]').val()).isValid()) {
                    $('#general_view input[name=sundayHoursFrom]').parent('fieldset').addClass('invalid');
                }
                if (!moment('1970-01-01T' + $('#general_view input[name=sundayHoursTo]').val()).isValid()) {
                    $('#general_view input[name=sundayHoursTo]').parent('fieldset').addClass('invalid');
                }
            } else {
                return false;
            }

            var Dfrom = new Date($('#general_view [name=\'dateFrom\']').val());
            var Dto = new Date($('#general_view [name=\'dateTo\']').val());
            // If dates are in future and
            if ( Dfrom <= Dto && (moment(Dfrom).unix() >= moment().startOf('day').unix() && moment(Dto).unix() >= moment().startOf('day').unix()) ) {
                if (Math.abs(moment.duration(moment(Dfrom).diff(moment(Dto))).asDays()) > 14) {
                    Navigation.alert(Language.scheme.alerts.coupon_exceeds_two_weeks);
                    $('#general_view [name="dateTo"], #general_view [name="dateFrom"]').parent('fieldset').addClass('invalid');
                }
            }

            if ($('#general_view input[name="category"]:checked').length == 0) {
                Navigation.alert(Language.scheme.alerts.categories_not_chosen, function(){
                    $.scrollTo( document.getElementById('categories_list').offsetTop, OFFSET_TOP_SPEED)
                });

                return false;
            } else {
                formData['categoryIds'] = formData['category'].join(',');
                delete formData['category'];
            }

            if (newBlobImg == '' || newBlobImg == undefined) {
                if ($("#general_view #createCouponForm #imageField #smallImage").attr('src') == '') {
                    // formData['couponImage'] = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAK1JREFUeNqUkjEKxiAMhZtoezkXB0/g0isUuvcm3cRRipuXsuP/pxRKCEVqwOGTPN8LEYwxQ09pROwTUHUJBmvtj9W+7w10zqFSiusBoIEUB0UkMZLAcRw1nZRSrfW+IsMQwtMh8Hrde89TxhgbOM8zkgM3FSMJpGacpqlLoElQSjnP8+k4joMLOF5xlmXhKXPODVzXtW+Glz20BYSwbRt5ffxHtHj43n3XX4ABALq6m4GkkftzAAAAAElFTkSuQmCC';
                    formData['couponImage'] = 'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAK1JREFUeNqUkjEKxiAMhZtoezkXB0/g0isUuvcm3cRRipuXsuP/pxRKCEVqwOGTPN8LEYwxQ09pROwTUHUJBmvtj9W+7w10zqFSiusBoIEUB0UkMZLAcRw1nZRSrfW+IsMQwtMh8Hrde89TxhgbOM8zkgM3FSMJpGacpqlLoElQSjnP8+k4joMLOF5xlmXhKXPODVzXtW+Glz20BYSwbRt5ffxHtHj43n3XX4ABALq6m4GkkftzAAAAAElFTkSuQmCC';
                } else {
                    delete formData['couponImage'];
                }
            } else {
                formData['couponImage'] = newBlobImg;
            }

            if (newBlobImgLogo == '' || newBlobImgLogo == undefined) {
                if ($("#general_view #createCouponForm #imageFieldLogo #smallImageLogo").attr('src') == '') {
                    formData['logo'] = 'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAK1JREFUeNqUkjEKxiAMhZtoezkXB0/g0isUuvcm3cRRipuXsuP/pxRKCEVqwOGTPN8LEYwxQ09pROwTUHUJBmvtj9W+7w10zqFSiusBoIEUB0UkMZLAcRw1nZRSrfW+IsMQwtMh8Hrde89TxhgbOM8zkgM3FSMJpGacpqlLoElQSjnP8+k4joMLOF5xlmXhKXPODVzXtW+Glz20BYSwbRt5ffxHtHj43n3XX4ABALq6m4GkkftzAAAAAElFTkSuQmCC';
                } else {
                    delete formData['logo'];
                }
            } else {
                formData['logo'] = newBlobImgLogo;
            }


            var validationFields = {};

            //original ['couponNumber', 'workdays', 'sunday', 'workdaysHoursFrom', 'workdaysHoursTo', 'sundayHoursFrom', 'sundayHoursTo', 'couponImage'];
            var ignoredFields = [
                'sunday',
                'sundayHoursFrom',
                'sundayHoursTo',
                'maxClicks',
                'clickPrice',
                'maxBudget',
                'generalBonus',
                'receiveBonusUpdates',
                'publishOnWall'
            ];
            var valid = true;

            for (var i in formData) {
                if (ignoredFields.indexOf(i) == -1) {
                    validationFields[i] = formData[i];
                }
            }

            if ($('#general_view .invalid').length == 0) {
                API._post('/coupons/validation?page=1', function(e, statusCode) {
                    if (statusCode == 400) {
                        var field_names = [];

                        $('#general_view input[type!="submit"]').each(function(){
                            field_names.push($(this).attr('name'));
                        });

                        for (var i in e) {
                            var elm = $('#general_view [name="' + e[i].param + '"]');

                            if (elm.length != 0 && ignoredFields.indexOf(e[i].param) == -1) {
                                elm.addClass('oops');
                                valid = false;
                            }

                            if (e[i].param == 'lat' || e[i].param == 'lng') {
                                $('#general_view a.show_map').addClass('oops');
                            }
                        }
                    }
                }, validationFields, false);
            } else {
                valid = false;
            }

            if (valid == false) {
                return false;
            }

            $('#general_view [name=logo]').appendTo('#temp_storage');

            if (typeof editId != 'undefined') {
                formData['coupon_status'] = couponData.status;
                localStorage.editCoupon = JSON.stringify(formData);
                localStorage.editId = editId;
				Navigation.changePage('limit');
            } else {
                formData['coupon_status'] = Language.scheme.messages.new_coupon;
                localStorage.createCoupon = JSON.stringify(formData);
				Navigation.changePage('limit');
            }

            // Navigation.changePage('limit');
        }, 1000);
    });
};

var pictureSource;   // picture source
var pictureSourceLogo;   // picture source
var destinationType; // sets the format of returned value
var destinationTypeLogo; // sets the format of returned value
var newBlobImg;
var newBlobImgLogo;

// Wait for device API libraries to load
//
document.addEventListener("deviceready", onDeviceReady, false);
document.addEventListener("deviceready", onDeviceReadyLogo, false);

// device APIs are available
//
function onDeviceReady() {
    pictureSource = navigator.camera.PictureSourceType;
    destinationType = navigator.camera.DestinationType;
}

function onDeviceReadyLogo() {
    pictureSourceLogo = navigator.camera.PictureSourceType;
    destinationTypeLogo = navigator.camera.DestinationType;
}

// Called when a photo is successfully retrieved
//
function onPhotoDataSuccess(imageData) {
    // Uncomment to view the base64-encoded image data
    // Get image handle
    //
    var smallImage = document.getElementById('smallImage');

    // Unhide image elements
    //
    smallImage.style.display = 'block';

    // Show the captured photo
    // The inline CSS rules are used to resize the image
    //
    var newImg = "data:image/png;base64," + imageData;
    smallImage.src = newImg;
    $("#Image2").attr("src", newImg);
    newBlobImg = imageData;
}

function onPhotoDataSuccessLogo(imageData) {
    // Uncomment to view the base64-encoded image data
    // Get image handle
    //
    var smallImage = document.getElementById('smallImageLogo');

    // Unhide image elements
    //
    smallImage.style.display = 'block';

    // Show the captured photo
    // The inline CSS rules are used to resize the image
    //
    var newImg = "data:image/png;base64," + imageData;
    smallImage.src = newImg;
    $("#Image2Logo").attr("src", newImg);
    newBlobImgLogo = imageData;
}

// Called when a photo is successfully retrieved
//
function onPhotoURISuccess(imageURI) {
    // Uncomment to view the image file URI
    // Get image handle
    //
    var largeImage = document.getElementById('largeImage');

    // Unhide image elements
    //
    largeImage.style.display = 'block';

    // Show the captured photo
    // The inline CSS rules are used to resize the image
    //
    largeImage.src = imageURI;
}

function onPhotoURISuccessLogo(imageURI) {
    // Uncomment to view the image file URI
    // Get image handle
    //
    var largeImage = document.getElementById('largeImageLogo');

    // Unhide image elements
    //
    largeImage.style.display = 'block';

    // Show the captured photo
    // The inline CSS rules are used to resize the image
    //
    largeImage.src = imageURI;
}

// A button will call this function
//
function capturePhoto() {
    // Take picture using device camera and retrieve image as base64-encoded string
    navigator.camera.getPicture(onPhotoDataSuccess, onFailGetPhoto, {
        quality: 50,
        destinationType: destinationType.DATA_URL,
        sourceType: 0
    });
}

function capturePhotoLogo() {
    // Take picture using device camera and retrieve image as base64-encoded string
    navigator.camera.getPicture(onPhotoDataSuccessLogo, onFailGetPhoto, {
        quality: 50,
        destinationType: destinationType.DATA_URL,
        sourceType: 0
    });
}

// A button will call this function
//
function capturePhotoEdit() {
    // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
    navigator.camera.getPicture(onPhotoDataSuccess, onFail, {
        quality: 50, allowEdit: true,
        destinationType: destinationType.DATA_URL
    });
}


function capturePhotoEditLogo() {
    // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
    navigator.camera.getPicture(onPhotoDataSuccessLogo, onFail, {
        quality: 50, allowEdit: true,
        destinationType: destinationType.DATA_URL
    });
}
// A button will call this function
//
function getPhoto(source) {
    // Retrieve image file location from specified source
    navigator.camera.getPicture(onPhotoDataSuccess, onFail, {
        quality: 50,
        destinationType: destinationType.DATA_URL,
        sourceType: source
    });
}

function getPhotoLogo(source) {
    // Retrieve image file location from specified source
    navigator.camera.getPicture(onPhotoDataSuccessLogo, onFail, {
        quality: 50,
        destinationType: destinationType.DATA_URL,
        sourceType: source
    });
}

// Called if something bad happens.
//
function onFail(message) {
    alert('Failed because: ' + message);
}