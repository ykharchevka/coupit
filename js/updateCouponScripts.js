﻿if (showWarningAlertOnLeave)
    window.onbeforeunload = closeEditorWarning;

function closeEditorWarning() {
    return 'לחזרה שלב אחורה יש ללחוץ על החץ בחלקו העליון של המסך! אם תבחר לסגור את תהליך יצירת הקופון הנתונים ימחקו. האם אתה בטוח שברצונך לצאת??'
}

function changeContact(e) {
    if (!$(e).val())
        $(e).css({ "border-color": "red", "border-width": "2px" });
    else
        $(e).css({ "border-color": "", "border-width": "" });
}


$(function () {

    var couponId = location.search.split('id=')[1];
    $("[name='couponId']").val(couponId);

    $.ajax({
        type: "POST",
        url: ipAddress + "/businessClientUpdateCouponPage",
        data: { couponId: couponId },
        xhrFields: {
            withCredentials: true
        },
        success: function (e) {

            if (e.couponDetails) {
                $("[name='couponNumber']").val(e.couponDetails.couponNumber);
                $("#couponNumberP").text(e.couponDetails.couponNumber);
                $("[name='couponName']").val(e.couponDetails.couponName);
                $("#Image1").attr("src", e.couponDetails.couponImage);
                $("[name='MarketingStatement']").val(e.couponDetails.MarketingStatement);
                $("[name='couponDescription']").text(e.couponDetails.couponDescription);
                $("[name='smallLetters']").text(e.couponDetails.smallLetters);
                $("[name='cellPrice']").val(e.couponDetails.cellPrice);
                $("[name='tariff']").val(e.couponDetails.tariff);
                $("[name='dateFrom']").val(htmlDateInputFormat(e.couponDetails.dateFrom));
                $("[name='dateTo']").val(htmlDateInputFormat(e.couponDetails.dateTo));
                $("[name='storeName']").val(e.couponDetails.storeName);
                $("[name='contactName']").val(e.couponDetails.contactName);
                $("[name='contactPhone']").val(e.couponDetails.contactPhone);
                $("[name='storePhone']").val(e.couponDetails.storePhone);
                $("[name='storeEmail']").val(e.couponDetails.storeEmail);
                $("[name='linkToProduct']").text(e.couponDetails.linkToProduct);
                $("[name='openOur']").text(e.couponDetails.openOur);
                $("[name='linkToFacebook']").text(e.couponDetails.linkToFacebook);
                $("[name='tapAmount'] option").each(function () {
                    if ($(this).text() == e.couponDetails.tapAmount)
                        $(this).attr("selected");
                });
                $("[name='tapPrice']").val(e.couponDetails.tapPrice);
                $("[name='maxBudget']").val(e.couponDetails.maxBudget);
                $("[name='generalBonus']").text(e.couponDetails.generalBonus);
                $("#Image2").attr("src", e.couponDetails.couponImage);
                $("[name='receiveBonusUpdates']").text(e.couponDetails.receiveBonusUpdates);
                $("[name='publishOnWall']").text(e.couponDetails.publishOnWall);
            }
        }, error: function (err) {
            //alert('אופס... משהו השתבש. נסה לרענן את הדף.');
        }
    });


    //image handlers
    $('#image_a').click(function () {
        $('#File_image').trigger('click');
    });
    $('#File_image').change(function () {
        //$('#imagePlace').css({'background':''});
        var file = $('#File_image')[0];
        if (file.files && file.files.length > 0) {
            var fr = new FileReader();
            fr.onload = function (e) {
                var basecode = e.target.result;
                //$('#imagePlace').css({ 'background-image': 'url(' + basecode + ')', 'background-position': 'center', 'background-repeat': 'no-repeat', 'background-size': 'cover' });
                $('#Image1').attr('src', basecode).css({ 'width': '100%' });
                $('#Image2').attr('src', basecode).css({ 'width': '100%' });
                var PH = $('#Image1').height();
                var PW = $('#Image1').width();
                $('#Pimage').css({ 'width': '100%', 'position': 'absolute', 'margin-top': PH / 2, 'margin-left': PW });
            }
            fr.readAsDataURL(file.files[0])
        }

    });

    var validation;
    function valid() {
        validation = "";
        if (!$("[name='couponName']").val())
            validation = "חובה למלא שם קופון!\n";
        if (!$("[name='cellPrice']").val())
            validation += "חובה למלא מחיר מבצע!\n";
        if (!$("[name='tariff']").val())
            validation += "חובה למלא מחיר מחירון!\n";
        if (!$("[name='dateFrom']").val())
            validation += "חובה למלא מתאריך!\n";
        if (!$("[name='dateTo']").val())
            validation += "חובה למלא עד תאריך!\n";
        if (!$("[name='contactPhone']").val())
            validation += "חובה למלא טלפון איש קשר!\n";
        if (!$("[name='storePhone']").val())
            validation += "חובה למלא טלפון בחנות!\n";
        if (!$("[name='storeEmail']").val())
            validation += 'חובה למלא דוא"ל בחנות!\n';

        $(".required").each(function () {
            if (!$(this).val())
                $(this).css({ "border-color": "red", "border-width": "2px" });
        });
    };



    $('.backTo1').click(function () {
        $('#step2').fadeOut("slow", function () {
            $('#step1').fadeIn("slow", function () {
            });
        });
    });
    $('.backTo2').click(function () {
        $('#step3').fadeOut("slow", function () {
            $('#step2').fadeIn("slow", function () {
            });
        });
    });
    $('#endStep1').click(function () {
        valid();

        if (validation == "") {
            $('#step1').fadeOut("slow", function () {
                $('#step2').fadeIn("slow", function () {
                });
            });
        }
        else
            alert(validation);
    });
    $('#endStep2').click(function () {
        $('#step2').fadeOut("slow", function () {
            $('#step3').fadeIn("slow", function () {
            });
        });
    });
    $('#endStep3').click(function () {
        ajx();
    });

    function ajx() {

        var formData = new FormData();
        var SF = $('#createCouponAllForms form').serializeArray();
        for (var i = 0; i < SF.length; i++) {
            formData.append(SF[i].name, SF[i].value);
        }
        var imagefile = $('#File_image')[0];
        formData.append('couponImage', imagefile.files[0]);

        $.ajax({
            type: "POST",
            url: ipAddress + "/businessClientUpdateCoupon",
            xhrFields: {
                withCredentials: true
            },
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (e) {

                alert(Language.scheme.alerts.coupon_successfully_updated);
                Navigation.changePage('coupon', {id: $("[name='couponId']").val()})
                // document.location.href = "coupon.htm?id=" + $("[name='couponId']").val();

            }, error: function (err) {
                //alert('אופס... משהו השתבש. אנא נסה שנית.');
            }

        });
    }


});